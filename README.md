# Oral French COreference Resolution System (OFCORS)


---
**WARNING**

**OFCORS currently doesn't work on Windows OS because of a jsonnet Python module dependancy, used by DECOFRE.**

---


OFCORS is a Python framework for coreference resolution in oral french text.  
It provides :

1. an end-to-end system to extract coreference chains from oral french transcribed text,

2. several turn-key tools to process the datas, or train your own ofcors model,

3. a reusable, and easy to understand API based on a modular architecture, useful for your own projects.


## Prerequisites:  

1. Python 3.8+

2. Decofre (`https://github.com/LoicGrobol/decofre/`) for French mention detection and the pre-trained model (c.f. the [Pre-Trained Models](#pre-trained-models) section).

3. Scorch (`https://github.com/LoicGrobol/scorch/`) for coreference chains scoring.

4. Optional (but highly recommended) : A Conda or Virtualenv virtual environnement.

5. SpaCy (>=3.0.5) (and french models "fr_core_news_sm", "fr_core_news_lg" and "fr_dep_news_trf").

6. Stanza (>=1.2) (and french model 'fr').

7. Spacy-stanza (>= 1.0.0)


## Installation

### Repository cloning

Into a terminal, type 
```console
[user@comp][~/path/to]$ git clone https://gitlab.com/Stanoy/ofcors.git`
[user@comp][~/path/to]$ cd ofcors/
```

### Environment management and dependencies


If you use a Conda virtual environment, you can create a new conda environment from the `environment.yml` file.
```console
[user@comp][~/path/to/ofcors]$ conda env create -f environment.yml
[user@comp][~/path/to/ofcors]$ conda activate ofcors_env
```
Else, you still can use the `requirements.txt` file to install all the required dependencies with pip.
```console
[user@comp][~/path/to/ofcors]$ pip install -r requirements.txt
```

### Flit installation

If you don't have it, install Flit with : 
```console
[user@comp][~/path/to/ofcors]$ pip install flit
```

To install OFCORS, there are two options availables to you.


The first one, consists in building a wheel file, and install ofcors with pip.
```console
[user@comp][~/path/to/ofcors]$ flit build --format wheel
[user@comp][~/path/to/ofcors]$ pip install dist/ofcors-X.Y.Z-py3-none-any.whl
```

The second one consists in using the `flit install` command
```console
[user@comp][~/path/to/ofcors]$ flit install -s 
[user@comp][~/path/to/ofcors]$ flit install --pth-file # For Windows users
```

### Pre-trained models

To fully enjoy the "OFCORS full 3D experience" (:wink:), you need to download some pre-trained models.

#### Mention Detector
Download the DECOFRE mention detection pre-trained model [here](https://github.com/LoicGrobol/decofre/releases/download/v0.7.0dev0/ANCOR-EVERYTHING-2021-01-13-detector.model) and save it anywhere below your home directory.

#### Pairs Classifiers
To get the best pre-trained pairs classifiers models, check the lastest release of the project's Gitlab Repository [here](https://gitlab.com/Stanoy/ofcors/-/releases), and save them anywhere below your home directory.

---
**NOTE**

It is better NOT to rename the models filenames and to save them below your home dir, so the MentionDetector and PairsClassifiers modules will find them automatically in your file system.
If not, you still can specify the path of the model as a parameter of the modules (c.f. the [doc](#ofcors-documentation)).

---


## OFCORS Usage

OFCORS provides a fully modular API and a turn-key and end-to-end text to chains system.

### From text to coreference-chains
Use the CLI `ofcors-infer` to extract the coreference chains from the raw text input.
By default, the files containing the results of each processing step are stored in the `./ofcors_outputs` directory. You can adjust it with the `-o` option.
```console
[user@comp][~/path/to/ofcors]$ ofcors-infer "Le week-end du 6 mai, Paul prendra seize ans. Il habite près du beffroi de Lyon avec sa mère Monique. Même si elle est dure avec lui, elle le fait pour son bien, car elle aime beaucoup son fils."
[user@comp][~/path/to/ofcors]$ ofcors-infer -f path/to/raw_text.txt
[user@comp][~/path/to/ofcors]$ ofcors-infer --help
Usage: ofcors-infer [OPTIONS] TEXT_OR_PATH

Options:
  -t, --text
  -f, --file
  -d, --mention-detector PATH
  -k, --tokenizer TEXT         [default: spacy]
  -o, --output-dir PATH        [default: ./ofcors_outputs]
  -p, --pairs-mode TEXT        [default: window]
  -w, --window-size INTEGER    [default: 30]
  -c, --classifier TEXT        [default: multi]
  -m, --chaining-mode TEXT     [default: best_first]
  --help                       Show this message and exit.
```

### Modular API

#### ANCOR parsing

OFCORS also provides an ANCOR parsing packages, allowing to parse and extract the datas from the ANCOR datas contained in XML-TEI files.
All the class and methods of this package are available in `ofcors.ancorparsing`

#### Pairwise corefrence detection modules

Each of the modules composing the end-to-end systems are fully reusable, detachable and plug-able at your convenience, just as puzzle pieces.
They are callable, and their properties can be specified at instantiation.
Those modules are available in `ofcors.pairwise.modules`.

#### Functional API

OFCORS provides a functional API as well. Each modules of the modular API find its equivalent functions.
This functional API is available in `ofcors.pairwise.functional`.

### Exemples

A little step-by-step exemple you can play with as a base to understand how each module works.

```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 27 13:40:02 2021

@author: AZZOUZA Théo
"""

#%% Imports
import ofcors
from ofcors.pairwise.modules import MentionDetector
from ofcors.pairwise.modules import PairsGenerator
from ofcors.pairwise.modules import DataframeBuilder
from ofcors.pairwise.modules import PairsClassifier
from ofcors.pairwise.modules import Chainer
print(ofcors.__version__)

#%% Creation des modules
d = MentionDetector()
p = PairsGenerator()
dfb = DataframeBuilder()
c = PairsClassifier()
chainer = Chainer()

#%% Detection des mentions
res_d = d("Le week-end du 6 mai, Paul prendra seize ans. Il habite près du beffroi de Lyon avec sa mère Monique. Même si elle est dure avec lui, elle le fait pour son bien, car elle aime beaucoup son fils.")
print(res_d)

#%% Creation des paires
res_p = p(res_d)
print(res_p[0])
print(res_p[1])

#%% Creation d'un dataframe de paires avec calcul des traits
df = dfb(*res_p)

#%% Classification des paires
p_df = c(df)
print(p_df)

#%% Chaînage des paires (par défaut en closest-first)
chainer = Chainer()
chains = chainer(p_df)
print(chains)

#%% Embranchement du pipeline complet
res = chainer(c(dfb(*p(d("Paul a quatre ans, il habite à Lyon avec sa mère qui l'aime beaucoup. Même si elle est dure avec lui, elle le fait pour son bien.")))))
print(res)
```

## OFCORS documentation

The detailed OFCORS documentation is available in the docs directory.

To read it, open the `docs/_build/html/index.html` with your favorite browser.
For exemple, with CLI :
```console
[user@comp][~/path/to/ofcors]$ firefox docs/_build/html/index.html
```
