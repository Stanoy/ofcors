ofcors package
==============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   ofcors.ancorparsing
   ofcors.pairwise
   ofcors.utils

Module contents
---------------

.. automodule:: ofcors
   :members:
   :undoc-members:
   :show-inheritance:
