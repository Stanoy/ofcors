ofcors.pairwise package
=======================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   ofcors.pairwise.datasets
   ofcors.pairwise.functional
   ofcors.pairwise.modules

Submodules
----------

ofcors.pairwise.ofcorspairwiseinfer module
------------------------------------------

.. automodule:: ofcors.pairwise.ofcorspairwiseinfer
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: ofcors.pairwise
   :members:
   :undoc-members:
   :show-inheritance:
