# For parsing
from lxml import etree
from ._teiid import TeiId
# For chains computing
import networkx as nx
# For sorting
import functools
# For ordereddict
import collections
# Debugging
import time
# Strings processing
import re



class XMLTEIFileReader():
    """ Classe XMLTEIFileReader.

    Décrit un lecteur de fichier de corpus ANCOR au format XML-TEI.
    Un lecteur de fichier permet de lire un fichier de corpus ANCOR se trouvant dans un sous-corpus donné,
    et permet d'en extraire l'ensemble des mentions et des relations de coréférences au format JSON.
    """
    
    def __init__(self, file_path):
        """ Constructeur de ANCORFileReader.
        
        Construit un lecteur de fichier de corpus ANCOR en initialisant ses attributs aux valeurs passées en argument.
        
        Parameters
        ----------
        file_path : str
            Chemin vers le fichier à analyser par l'objet.
        """
        self.TEI = "{http://www.tei-c.org/ns/1.0}"
        self.XML = "{http://www.w3.org/XML/1998/namespace}"
        self.NSMAP = {"tei": self.TEI[1:-1], "xml": self.XML[1:-1]}
        self.file_path = file_path
        self.tree = etree.parse(file_path)
        self.root = self.tree.getroot()

        

    def get_mentions(self, include_discontinuous=True):
        """ Méthode get_mentions.
        
        Génère au format JSON l'ensemble des mentions se trouvant dans le fichier de corpus ANCOR de l'objet.
        Les mentions sont retournées dans leur ordre d'apparition dans le texte.
        
        Parameters
        ----------
        include_discontinuous : boolean
            Détermine si les mentions discontinues du fichier doivent être considérées ou non.
            Vaut `True` par défaut.
        
        Returns
        -------
        dict
            Le dictionnaire ordonné contenant l'ensemble des mentions se trouvant dans le fichier de corpus ANCOR de l'objet.
        """
        json_mentions = {}

        # -- MENTIONS CONTINUES 
        mentions_group_node = self.root.find('.//tei:spanGrp[@tei:subtype="mention"]', namespaces=self.NSMAP)
        for mention_desc in mentions_group_node.iter(f'{self.TEI}span'):
            data_character = {}
            
            cur_ana = str(mention_desc.attrib[f"{self.TEI}ana"]) if (str(mention_desc.attrib[f"{self.TEI}ana"])[0] != '#') else str(mention_desc.attrib[f"{self.TEI}ana"])[1:]
            data_character["OBJ_TYPE"] = str(mentions_group_node.attrib[f"{self.TEI}type"])
            data_character["OBJ_SUBTYPE"] = str(mentions_group_node.attrib[f"{self.TEI}subtype"])
            data_character["ANA"]  = str(cur_ana)
            
            # Stockage des identifiants des mots de début et de fin de la mention
            data_character["START_ID"] = str(mention_desc.attrib[f'{self.TEI}from'])
            data_character["END_ID"] = str(mention_desc.attrib[f'{self.TEI}to'])
            
            # Pour chaque mention, on cherche à analayser ses caractéristiques et à les stocker.
            cur_mention_fs_node = self.root.find(f'.//tei:fs[@xml:id="{cur_ana}"]', namespaces=self.NSMAP)
            for feature in cur_mention_fs_node.iter(f'{self.TEI}f'):
                feature_name = str(feature.attrib[f'{self.TEI}name']).upper()
                feat_value=feature.findtext(f'{self.TEI}string')
                if feat_value is None:
                    feat_value=feature.find(f'{self.TEI}symbol').attrib[f'{self.TEI}value']
                data_character[feature_name] = str(feat_value)
            
            # Ajout du contenu de la mention s'il n'est pas annoté dans le corpus.
            if "CONTENT" not in list(data_character.keys()) or data_character["CONTENT"].strip() == "":
                data_character["CONTENT"] = " ".join(self.get_words_in(TeiId(data_character["START_ID"]), TeiId(data_character["END_ID"]), inclusive=True))
            
            # Dans tous les cas, on trim la chaîne de caractère pour la rendre la plus propre possible.
            data_character["CONTENT"] = re.sub(' +', ' ', data_character["CONTENT"].strip())
            
            json_mentions[str(mention_desc.attrib[f"{self.XML}id"])] = data_character
          
        if include_discontinuous:  
            # -- MENTIONS DISCONTINUES
            discontinuous_mentions_group_node = self.root.find('.//tei:spanGrp[@tei:subtype="mention.discontinuous"]', namespaces=self.NSMAP)
            for mention_desc in discontinuous_mentions_group_node.iter(f'{self.TEI}span'):
                data_character = {}
                
                cur_ana = str(mention_desc.attrib[f"{self.TEI}ana"]) if (str(mention_desc.attrib[f"{self.TEI}ana"])[0] != '#') else str(mention_desc.attrib[f"{self.TEI}ana"])[1:]
                data_character["OBJ_TYPE"] = str(discontinuous_mentions_group_node.attrib[f"{self.TEI}type"])
                data_character["OBJ_SUBTYPE"] = str(discontinuous_mentions_group_node.attrib[f"{self.TEI}subtype"])
                data_character["ANA"]  = str(cur_ana)
                
                # Stockage des identifiants des mots de début et de fin de la mention
                tokens_tei_ids = str(mention_desc.attrib[f'{self.TEI}target']).split()
                data_character["START_ID"] = tokens_tei_ids[0]
                data_character["END_ID"] = tokens_tei_ids[-1]
                
                # Pour chaque mention, on cherche à analayser ses caractéristiques et à les stocker.
                cur_mention_fs_node = self.root.find(f'.//tei:fs[@xml:id="{cur_ana}"]', namespaces=self.NSMAP)
                for feature in cur_mention_fs_node.iter(f'{self.TEI}f'):
                    feature_name = str(feature.attrib[f'{self.TEI}name']).upper()
                    feat_value=feature.findtext(f'{self.TEI}string')
                    if feat_value is None:
                        feat_value=feature.find(f'{self.TEI}symbol').attrib[f'{self.TEI}value']
                    data_character[feature_name] = str(feat_value)
                    
                # Remplissage du contenu des mentions discontinues en concaténant l'ensemble des tokens qui les composent
                content_string = ""
                for token_tei_id in tokens_tei_ids:
                    # Filtrage du '#' en début de texte
                    clean_id = token_tei_id if (token_tei_id[0] != '#') else token_tei_id[1:]
                    cur_token_node = self.root.find(f'.//tei:w[@xml:id="{clean_id}"]', namespaces=self.NSMAP)
                    if cur_token_node is not None: # Le cas None n'apparait que si le token est une ponctuation (balise pc) : on exclue les ponctuations, donc on n'ajoute pas au content
                        content_string += f" {cur_token_node.text}"
                data_character["CONTENT"] = re.sub(' +', ' ', content_string.strip())
                       
                json_mentions[str(mention_desc.attrib[f"{self.XML}id"])] = data_character
        
        # Tri des mentions et ajout du numéro (pour calcul des distances)
        json_mentions_sorted = XMLTEIFileReader.sort_mentions(json_mentions)
        for num, mention in enumerate(json_mentions_sorted.keys()):
            json_mentions_sorted[mention]['NUM'] = str(num)
        
        return json_mentions_sorted;
       
    
    
    def get_coreferences(self, coreference_types=["DIRECTE", "INDIRECTE", "ANAPHORE", "UNK"], first=True):
        """ Méthode get_coreferences.
        
        Génère au format JSON l'ensemble des relations de coréférences se trouvant dans le fichier de corpus ANCOR
        de l'objet.
        Seules les relations de coréférence de type appartenant à 'coreference_types' sont retournées.
        
        Parameters
        ----------
        coreference_types : list[str]
            La liste des types de coréférence à extraire du fichier de corpus ANCOR de l'objet.
        first : bool
            La méthode retourne les relations du fichier annotées en première mention si vrai, et les relations
            du fichier annotées en chaîne sinon.
            
        Returns
        -------
        dict
            Le dictionnaire contenant l'ensemble des relations de coréférence de type 'coreference_types'
            se trouvant dans le fichier de corpus ANCOR de l'objet.
            Exemple :
                {"r-COREFERENCE-jmuzerelle_1357822847399":
                     {
                         "OBJ_TYPE": "relation",
                         "OBJ_SUBTYPE": "coreference",
                         "ANA": "r-COREFERENCE-jmuzerelle_1357822847399-fs",
                         "TYPE": "DIRECTE",
                         "DISTANCE_CHAR": "305",
                         ...
                         "LEFT_NAME": "u-MENTION-jmuzerelle_1357739091627",
                         "RIGHT_NAME": "u-MENTION-jmuzerelle_1357739275052"
                     },
                 "r-COREFERENCE-jmuzerelle_1357822858952":
                     {
                         ...
                     },
                 ...
                }
        """
        _coreference_types = list(map(str.upper, coreference_types))
        json_corefs = {}

        # Parcours des descripteurs de relation
        if first:
            relations_group_node = self.root.find('.//tei:linkGrp[@tei:subtype="coreference"][@tei:description="first"]', namespaces=self.NSMAP)
        else :
            relations_group_node = self.root.find('.//tei:linkGrp[@tei:subtype="coreference"][@tei:description="chains"]', namespaces=self.NSMAP)
            
        for relation_desc in relations_group_node.iter(f'{self.TEI}link'):
            data_character = {}
            cur_ana = str(relation_desc.attrib[f"{self.TEI}ana"]) if (str(relation_desc.attrib[f"{self.TEI}ana"])[0] != '#') else str(relation_desc.attrib[f"{self.TEI}ana"])[1:]
            cur_relation_fs_node = self.root.find(f'.//tei:fs[@xml:id="{cur_ana}"]', namespaces=self.NSMAP)
            # Si le type de la coréférence ne fait pas partie des types de coréfs à garder, on la skipe
            if cur_relation_fs_node.find(f'.//tei:f[@tei:name="type"]', namespaces=self.NSMAP).find(f'{self.TEI}string').text.upper() in _coreference_types:
                # Caracs générales
                data_character["OBJ_TYPE"] = str(relations_group_node.attrib[f"{self.TEI}type"])
                data_character["OBJ_SUBTYPE"] = str(relations_group_node.attrib[f"{self.TEI}subtype"])
                data_character["ANA"]  = cur_ana
            
                # Pour chaque relation, on cherche à analayser ses caractéristiques et à les stocker.
                for feature in cur_relation_fs_node.iter(f'{self.TEI}f'):
                    feature_name = str(feature.attrib[f'{self.TEI}name']).upper()
                    feat_value = feature.findtext(f'{self.TEI}string')
                    data_character[feature_name] = str(feat_value)
           
                # Stockage des deux identifiants des mention de la relation de coreference
                targets = str(relation_desc.attrib[f'{self.TEI}target']).split()
                data_character["LEFT_NAME"] = targets[0] if (targets[0][0] != '#') else targets[0][1:]
                data_character["RIGHT_NAME"] = targets[1] if (targets[1][0] != '#') else targets[1][1:]
                    
                json_corefs[str(relation_desc.attrib[f"{self.XML}id"])] = data_character
        
        return json_corefs;
    
    
    def get_chains(self):
        json_corefs_chains = {}
        
        # Parcours des descripteurs de relation
        chains_group_node = self.root.find('.//tei:linkGrp[@tei:subtype="chain"]', namespaces=self.NSMAP)
        for cur_chain_desc in chains_group_node.iter(f'{self.TEI}link'):
            chain_id = int(str(cur_chain_desc.attrib[f"{self.XML}id"]).split('-')[-1])
            chain_list = [name if name[0] != '#' else name[1:]
                          for name in str(cur_chain_desc.attrib[f"{self.TEI}target"]).split()]
            json_corefs_chains[chain_id] = chain_list
        
        # Ordonnancement des chaines
        json_corefs_chains = {k:json_corefs_chains[k] for k in sorted(list(json_corefs_chains.keys()))}
        
        return json_corefs_chains;
        
    
    def compute_chains(self, json_corefs, sorted_mentions_dict=None):
        """ Méthode generate_json_chains.
        
        Génère au format JSON l'ensemble des chaînes de coréférences de l'ensemble des coréférences décrites
        au format JSON par 'json_corefs'.
        
        Parameters
        ----------
        json_corefs : dict
            L'ensemble des relations de coréférences dans lequel chercher l'ensemble des chaînes de coréférences.
        sorted_mentions_dict : dict, optional.
            Dictionnaire decrivant les mentions des liens de coréferences selon leur
            ordre d'apparition dans le texte.
                    
        Returns
        -------
        dict
            Le dictionnaire contenant pour chaque chaîne de coréférence,
            l'ensemble des mentions qui la composent selon leur ordre d'apparition
            dans le texte.
        """
        json_corefs_chains = {}
        
        if sorted_mentions_dict is None:
            sorted_mentions_dict = self.get_mentions()
        
        G = nx.Graph()
        
        # Construction du graphe des relations entre les mentions coréférentes
        for relation in list(json_corefs.keys()):
            left_name = json_corefs[relation]["LEFT_NAME"]
            right_name = json_corefs[relation]["RIGHT_NAME"]
            G.add_edge(left_name, right_name)

        # Parcours des composantes connexes du graphe
        # 1 CC <=> une chaîne de coréférence
        chain_id = 0
        for subgraph in list(nx.connected_components(G)):
            chain_mentions_names = set()
            for node in subgraph:
                chain_mentions_names.add(node)
            # Mise dans l'ordre des mentions de la chaîne, pour la selection consecutive.
            sorted_chain_mentions_names = [k for k in list(sorted_mentions_dict.keys()) if k in chain_mentions_names]
            json_corefs_chains[chain_id] = sorted_chain_mentions_names
            chain_id += 1
            
        # Ajout de tous les singletons
        # Une mention qui n'est reliée à rien est un singleton
        for mention in list(sorted_mentions_dict.keys()):
            if mention not in list(G.nodes):
                json_corefs_chains[chain_id] = [mention]
                chain_id += 1
        
        return json_corefs_chains;
    
    
    
    def get_words_in(self, word_tei_id_1, word_tei_id_2, inclusive=False):
        """ Méthode get_words_in.
        
        Renvoie la liste des mots se trouvant dans l'intervalle ]'word_id_1' et 'word_id_2'[.
        L'argument 'inclusive' permet de définir l'ouverture de l'intervalle. Par défaut, l'intervalle est ouvert.
        
        Parameters
        ----------
        word_tei_id_1 : TeiId
            Identifiant de mot dans le fichier de corpus ANCOR de l'objet.
        word_tei_id_2 : TeiId
            Identifiant de mot dans le fichier de corpus ANCOR de l'objet.
        inclusive : bool
            Rend l'intervalle de recherche fermé (inclusif) si vrai, et ouvert (non-inclusif) sinon.
            Vaut False par défaut.
        
        Returns
        -------
        List<str>
            La liste ordonnée des mots contenus dans l'intervalle décrit par les identifiants de mots en arguments.
            
        Raises
        ------
        ValueError
            Si l'un des TeiId passés en argument n'est pas un TeiId de mot.
        """
        if word_tei_id_1.w is None or word_tei_id_2.w is None:
            raise ValueError('Les TeiId passés en argument doivent être des TeiId de mots.')
        
        # Comme le comparateur de TeiId renvoie la distance en mots des deux TeiId,
        # 0 => inclus la comparaison avec soi-même
        # 1 => Requiert un écart qu'au moins 1 de distance.
        inclusive_ = 0 if inclusive else 1
        
        words_list = []
        for word in self.root.iter(f"{self.TEI}w"):
            cur_w_tei_id = TeiId(str(word.attrib[f"{self.XML}id"]).lstrip('#'))
            # On ne prend que les mots compris dans [word_tei_id_1; word_tei_id_2] (ouvert ou fermé)
            if ((cur_w_tei_id.compare(word_tei_id_1) >= inclusive_) and (cur_w_tei_id.compare(word_tei_id_2) <= -inclusive_)):
                words_list.append(word.text)
        
        return words_list;
    
    
    
    def get_mentions_in(self, word_tei_id_1, word_tei_id_2, sorted_mentions_dict=None, inclusive=False):
        """ Méthode get_mentions_in.
        
        Renvoie la liste des mentions se trouvant dans l'intervalle ]'word_id_1' et 'word_id_2'[.
        L'argument 'inclusive' permet de définir l'ouverture de l'intervalle. Par défaut, l'intervalle est ouvert.
        
        Parameters
        ----------
        word_tei_id_1 : TeiId
            Identifiant de mot dans le fichier de corpus ANCOR de l'objet.
        word_tei_id_2 : TeiId
            Identifiant de mot dans le fichier de corpus ANCOR de l'objet.
        sorted_mentions_dict : dict, optional
            Le dictionnaire ordonné des mentions du texte.
        inclusive : bool, optional
            Rend l'intervalle de recherche fermé (inclusif) si vrai, et ouvert (non-inclusif) sinon.
            Vaut False par défaut.
        
        Returns
        -------
        List<str>
            La liste ordonnée des mentions contenues dans l'intervalle décrit par les identifiants de mots en arguments.
            
        Raises 
        ------
        ValueError
            Si l'un des TeiId passés en argument n'est pas un TeiId de mot.
        """
        # On calcule d'abord l'ensemble des mots dans l'intervalle
        words_list_upper_case = list(map(str.upper, self.get_words_in(word_tei_id_1, word_tei_id_2, inclusive)))
        
        # On calcule l'ensemble des mentions du fichier, si on ne les a pas.
        if sorted_mentions_dict is None:
            sorted_mentions_dict = self.get_mentions()
        
        # Pour chaque mention, on regarde si elles sont dans les mots de l'intervalle.
        mentions_list = []
        for mention in sorted_mentions_dict:
            if ("CONTENT" in sorted_mentions_dict[mention]) and (sorted_mentions_dict[mention]["CONTENT"] is not None):
                mention_text = sorted_mentions_dict[mention]["CONTENT"]
            else :
                mention_text = " ".join(self.get_words_in(TeiId(sorted_mentions_dict[mention]["START_ID"]), TeiId(sorted_mentions_dict[mention]["END_ID"]), True))
            if (mention_text.upper() in words_list_upper_case):
                mentions_list.append(sorted_mentions_dict[mention]["CONTENT"])
        
        return mentions_list;
            


    def get_sentence_of_mention(self, mention_tei_id):
        """ Méthode get_sentence_of_mention.
        
        Renvoie la phrase entière dans laquelle se trouve la mention décrite par son identifiant TeiId 'mention_id'
        dans le fichier de corpus ANCOR de l'objet.
        
        Parameters
        ----------
        mention_tei_id : TeiId
            Identifiant de la mention dans le fichier de corpus ANCOR de l'objet.
        
        Returns
        -------
        str
            La phrase entière dans laquelle se trouve la mention décrite par 'mention_id'.
        """
        u_tei_id_splitted = mention_tei_id.splitted_tei_id[:2]
        u_tei_id = TeiId.join_splitted_tei_id(u_tei_id_splitted)
        u_node = self.root.find(f'.//tei:u[@xml:id="{u_tei_id}"]', namespaces=self.NSMAP)
        words_list = []
        for word in u_node.iter():
            if ((word.tag == f'{self.TEI}w') or 
                ((word.tag == f'{self.TEI}pc') and (len(word.attrib[f"{self.XML}id"].split(".")) == 3))): # On ne prend que les ponctuations nécessaires
                words_list.append(word.text)
            
        return ' '.join(words_list);
    
    
    
    
    
    @staticmethod
    def sort_mentions(mentions_dict):
        """ Méthode statique sort_mentions.
        
        Trie les mentions décrites par 'mentions_dict' par ordre d'apparition dans le texte
        (i.e. par 'START_ID') et retourne le dictionnaire trié.
        
        Parameters
        ----------
        mentions_dict : dict
            Dictionnaire des mentions à trier.
        
        Returns
        -------
        dict
            Le dictionnaire des mentions triées par ordre d'apparition.
        """
        
        # Since Python 3.7, dict save the add order
        # On fait le choix de se conformer à Python 3.7.
        # Plus tard, les view objects retournés par keys() et values()
        # donneront BIEN les valeurs DANS L'ORDRE d'insertion.
        
        # Récupération des 'START_ID' des mentions
        dict_mentions_id = {mention_name:mentions_dict[mention_name]["START_ID"] for mention_name in mentions_dict.keys()}
        
        # Tri des mentions selon leur 'START_ID'
        sorted_mentions_names = [mention_name for mention_name, mention_id in 
                                sorted(dict_mentions_id.items(),
                                       key=functools.cmp_to_key(
                                           lambda c1, c2 : TeiId.compare_tei_id(TeiId(c1[1]), TeiId(c2[1]))
                                           ))]
        
        # Reconstruction du dictionnaire des mentions, dans l'ordre
        sorted_mentions_dict = {mention_name:mentions_dict[mention_name] for mention_name in sorted_mentions_names}
        
        return sorted_mentions_dict;
