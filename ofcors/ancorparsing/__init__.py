'''Oral French COreference Resolution System'''

from ._ancordataframebuilder import ANCORDataframeBuilder
from ._teiid import TeiId
from ._xmlteifilereader import XMLTEIFileReader

__all__ = ['ANCORDataframeBuilder', 'TeiId', 'XMLTEIFileReader']
