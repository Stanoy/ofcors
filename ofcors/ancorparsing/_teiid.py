class TeiId:
    """ Classe TeiId.
    
    Décrit des identifiants de mots ou de section de mots se trouvant dans les fichiers de corpus ANCOR au format XML-TEI.
    Exemple de format : "s1.u4.w10".
    Les TeiId sont capables de se comparer entre eux, et conservent les valeurs numériques
    de chacune de leurs parties : s, u et w (s'il s'agit d'un identifiant de mot dans le fichier XML-TEI).
    La classe TeiId fournit également des outils permettant une manipulation et une conversion aisée de ces identifiants, dans deux formats.
    Elle permet de comparer des TeiId, d'extraire les valeurs numériques d'un identifiant en chaîne de caractères,
    ou à l'inverse de former un identifiant sous-forme de chaîne de caractères à partir d'une list de valeurs numériques.
    
    Attributes
    ----------
    splitted_tei_id : list
        La liste contenant les valeurs numériques associées à chaque membre de l'identifiant (s, u, w).
    str_tei_id : str
        L'identifiant sous sa forme de chaîne de caractères, comme utilisées dans les fichiers de corpus ANCOR.
        Exemple : "s1.u4.w10".
    s : int
        La valeur numérique du membre s de l'identifiant.
    u : int
        La valeur numérique du membre u de l'identifiant.
    w : Union[int, None]
        La valeur numérique du membre w de l'identifiant. Peut valoir None si l'identifiant ne décrit pas un mot.
    
    Methods
    -------
    __init__(self, *args)
        Construit une instance de TeiId à partir d'une chaîne de caractère ou des valeurs numériques de chacun des membres s, u et w.
    
    compare(self, tei_id : TeiId)
        Compare l'objet avec un autre TeiId.
    
    Static Methods
    --------------
    split_string_tei_id(str_tei_id)
        Convertit une chaîne de caractère représentant un identifiant en une liste des valeurs numériques de chacun de ses membres.
        Opération inverse de string_splitted_tei_id.
        
    join_splitted_tei_id(splitted_tei_id)
        Convertit une liste de valeurs numériques des membres d'un identifiant en sa chaîne de caractère.
        Opération inverse de split_string_tei_id.
        
    compare_tei_id(tei_id_1 : TeiId, tei_id_2 : TeiId)
        Compare deux TeiId.
    """
    
    
    def __init__(self, *args):
        """ Constructeur de TeiId.
        
        Construit un identifiant de mot ou de section de mots du corpus ANCOR en initialisant ses attributs aux valeurs passées en argument.
        
        Parameters
        ----------
        *args : list
            Liste d'arguments pour construire un TeiId.
            Le constructeur accepte :
                - Une liste des valeurs numériques de s, u et w (optionel),
                - Une chaîne de caractère décrivant l'identifiant au format : sX.uY.wZ or sX.uY,
                - Les valeurs numériques de s, u et w données séparemment.
        """
        if len(args) == 1:
            if isinstance(args[0], list):      
                self.__initializeFromList(args[0])             
            elif isinstance(args[0], str):
                self.__initializeFromString(args[0])           
        elif ((len(args) == 2) or (len(args) == 3)):
            self.__initializeFromList(list(args))
        else:
            raise ValueError('TeiId constructor only accepts : \n - A list of the s, u, and (optionally) w int values,\n - A string XML-TEI identifier of shape : "sX.uY.wZ" or "sX.uY",\n - The s, u, w int values given separately.')
        
    
    
    def __initializeFromList(self, splitted_tei_id):
        """ Méthode __initializeFromString.
        
        Initialise l'objet à partir d'une liste des valeurs numériques des membres de l'identifiant.
        
        Parameters
        ----------
        splitted_tei_id : list
            liste des valeurs numériques des membres de l'identifiant.
        """
        if len(splitted_tei_id) <= 3 and len(splitted_tei_id) >= 2:
            self.splitted_tei_id = splitted_tei_id
            self.str_tei_id = TeiId.join_splitted_tei_id(self.splitted_tei_id)
            self.s = self.splitted_tei_id[0]
            self.u = self.splitted_tei_id[1]
            if len(self.splitted_tei_id) == 3:
                self.w = self.splitted_tei_id[2]
            else:
                self.w = None
        else :
            raise ValueError('The s, u and optionnaly w values are needed to build an instance of TeiId.')

    
    
    def __initializeFromString(self, str_tei_id):
        """ Méthode __initializeFromString.
        
        Initialise l'objet à partir d'une chaîne de caractère au format : "sX.uY.wZ" or "sX.uY."
        
        Parameters
        ----------
        str_tei_id : str
            La chaîne de caractère au format : "sX.uY.wZ" or "sX.uY".
        """
        splitted_tei_id = TeiId.split_string_tei_id(str_tei_id)
        if len(splitted_tei_id) <= 3 and len(splitted_tei_id) >= 2:
            self.__initializeFromList(splitted_tei_id)
        else :
            raise ValueError('The ANCOR XML-TEI string identifier should be of shape : "sX.uY.wZ" or "sX.uY".')
    
    
    
    
    
    @staticmethod
    def split_string_tei_id(str_tei_id):
        """ Méthode split_string_tei_id.
        
        Convertit une chaîne de caractère représentant un identifiant en une liste des valeurs numériques de chacun de ses membres.
        Opération inverse de string_splitted_tei_id.
        
        Parameters
        ----------
        str_tei_id : str
            La chaîne de caractère au format : "sX.uY.wZ" or "sX.uY".
        
        Returns
        -------
        list
            La liste contenant les valeurs numériques de chacun des membres de l'identifiant décrit par 'str_tei_id'.
        """
        return [int(elt.lstrip('suw')) 
                   for elt in str_tei_id.lstrip('#').split('.')]
    
    
    
    @staticmethod
    def join_splitted_tei_id(splitted_tei_id):
        """ Méthode join_splitted_tei_id.
        
        Convertit une liste de valeurs numériques des membres d'un identifiant en une chaîne de caractère.
        Opération inverse de split_string_tei_id.
        
        Parameters
        ----------
        splitted_tei_id : list
            La liste contenant les valeurs numériques de chacun des membres de l'identifiant
        
        Returns
        -------
        str
            La chaîne de caractère au format : "sX.uY.wZ" or "sX.uY", associée à 'splitted_tei_id'.
        """
        tei_id = ""
        component_dict = {0:"s", 1:".u", 2:".w"}
        for i, elt in enumerate(splitted_tei_id):
            tei_id+=component_dict[i]+str(elt)
        return tei_id;
    
    
    
    @staticmethod
    def compare_tei_id(tei_id_1, tei_id_2):
        """ Méthode statique compare_word_id.
        
        Compare deux TeiId.
        
        Parameters
        ----------
        word_id_1 : TeiId
            Identifiant dans un fichier de corpus ANCOR.
        word_id_2 : TeiId
            Identifiant dans un fichier de corpus ANCOR.
            
        Returns
        -------
        int
            La distance entre 'tei_id_1' et 'tei_id_2'.
            <0, si tei_id_1 < tei_id_2
            >0, si tei_id_1 > tei_id_2
            0  : sinon
            
        Raises
        ------
        ValueError
            Si les deux TeiId à comparer ne sont pas de la même longueur (i.e. s'ils ne décrivent pas la même entité).
            Ils ne sont alors pas comparables.
        """
        id_splitted_1 = tei_id_1.splitted_tei_id
        id_splitted_2 = tei_id_2.splitted_tei_id
        if len(id_splitted_1) == len(id_splitted_2):
            for elt1, elt2 in zip(tei_id_1.splitted_tei_id, tei_id_2.splitted_tei_id):
                if (elt1 != elt2):
                    return elt1 - elt2;
            return 0;
        else :
            raise ValueError("The two TeiId to compare must have the same length.")
    
    
    
    def compare(self, tei_id):
        """ Méthode compare.
        
        Compare l'objet avec un autre TeiId.
        
        Parameters
        ----------
        tei_id : TeiId
            Identifiant dans un fichier de corpus ANCOR à comparer.
            
        Returns
        -------
        int
            La distance entre 'self' et 'tei_id'.
            <0, si self < tei_id
            >0, si self > tei_id
            0  : sinon
            
        Raises
        ------
        ValueError
            Si les deux TeiId à comparer ne sont pas de la même longueur (i.e. s'ils ne décrivent pas la même entité).
            Ils ne sont alors pas comparables.
        """
        return TeiId.compare_tei_id(self, tei_id)
