from ._xmlteifilereader import XMLTEIFileReader
from ._teiid import TeiId
# For preconditions
from preconditions import preconditions
# Spacy model for mentions features computing
import spacy
# For data formatting
import json
import pandas as pd
# For combinations computing
import itertools
# For string computing
import re
import random
# For fs gestion
import glob
import os



class ANCORDataframeBuilder:
    """ Classe ANCORDataframeBuilder.

    Décrit les constructeurs de dataframes de paires à partir d'un ensemble de documents XML-TEI du corpus ANCOR.
    Un ANCORDataframeBuilder permet de construire des dataframes au format csv contenant des exemples de paires, coréférentes où non,
    et à partir d'un ensemble donné de documents XML-TEI du corpus ANCOR, avec pour chaque exemple, son ensemble de valeurs de traits.
    """

    def __init__(self, ancor_corpus_files, dataframe_output_folder, df_name):
        """ Constructeur de dataset_builder.
        
        Construit une fabrique de dataset ANCOR en initialisant ses attributs aux valeurs passées en argument.
        
        Parameters
        ----------
        ancor_corpus_files : str
            Repertoire ou liste de chemins de fichiers décrivant l'ensemble de documents XML-TEI du corpus ANCOR.
        dataframe_output_folder : str
            Chemin du repertoire dans lequel écrire le fichier au format csv contenant le dataframe.
            
        Raises
        ------
        ValueError
            Si ancor_corpus_files n'est ni un repertoire existant ni une liste de fichier tous existants.
        """
        
        if isinstance(ancor_corpus_files, (str, os.PathLike)) and os.path.isdir(ancor_corpus_files):
            self.ancor_corpus_files = [ os.path.join(ancor_corpus_files, f)
                                        for f in os.listdir(ancor_corpus_files) if (f.endswith(".tei") and os.path.isfile(os.path.join(ancor_corpus_files, f))) ]
        elif isinstance(ancor_corpus_files, list):
            if any(((not os.path.exists(file_path)) or (not os.path.isfile(file_path))) for file_path in ancor_corpus_files):
                raise ValueError("One of the specified file doesn't exist.")
            else:
                self.ancor_corpus_files = ancor_corpus_files
        else:
            raise ValueError("Argument ancor_corpus_files should be a directory or a list of file paths.")
        
        self.df_name = df_name
        self.dataframe_output_folder = dataframe_output_folder
        self.spacy_model = spacy.load("fr_dep_news_trf")
        
        
    
    @staticmethod
    def __weight(negative_pairs, positive_pairs, neg_pairs_percentage, absolute=False):
        """ Méthode de classe __weight.
        
        Balance le nombre d'exemples positifs et négatifs `positive_pairs` et `negative_pairs`
        respectivement, selon le taux `neg_pairs_percentage` d'exemples négatifs.
        Après calcul du nombre d'exemples négatifs à conserver (et de positifs en mode `absolute`)
        les nouveaux ensembles de paires positives et de paires négatives sont calculés
        par sous-échantillonnage aléatoire de leurs ensembles respectifs d'origine.
        
        Parameters
        ----------
        negative_pairs : set
            Ensemble de paires négatives à équilibrer.
        positive_pairs : set
            Ensemble de paires positives à équilibrer.
        neg_pairs_percentage : float
            Taux d'exemples négatifs à garder.
        absolute : bool, optional
            Détermine si la proportion dans chaque fenêtre doit être conservée de manière absolue ou non.
            Dans ce cas, s'il y a plus d'exemples positifs que d'exemples négatifs dans une fenêtre,
            le taux d'exemples positifs est également recalculé.
            Vaut False par défaut.

        Returns
        -------
        (set[tuple(str, str, int, int)], set[tuple(str, str, int, int)])
            Le couple `(negative_pairs, positive_pairs)` des ensembles pondérés des paires positives et négatives respectivement.
            Chaque paire est décrites par le nom de la mention gauche, le nom de la mention droite,
            la valeur 1 si la paire est coréférente, 0 sinon, et le numéro de leur chaîne si coréférente, -1 sinon.
            Exemple : ('u-MENTION-jmuzerelle_1354180728605', 'u-MENTION-jmuzerelle_1354180965453', 1, 5)

        """
        _negative_pairs = set(negative_pairs)
        _positive_pairs = set(positive_pairs)
        
        # -- Calcul du nombre d'exemples négatifs à garder en fonction du nombre de positifs générés
        num_positive_relations_to_keep = len(_positive_pairs)
        total_to_keep = int(round(len(_positive_pairs)/(1-neg_pairs_percentage)))
        num_negative_relations_to_keep = int(round(neg_pairs_percentage*total_to_keep))
        
        
        # SI le nombre d'exemple négatifs à garder est plus grand que le nombre de négatifs effectivement présents
        # ALORS on ajuste le nombre de paires négatives au maximum possible
        # ET on NE RECALCULE PAS le nombre de paires positives sauf en mode absolu
        if num_negative_relations_to_keep > len(_negative_pairs) :
            num_negative_relations_to_keep = len(_negative_pairs)
            if absolute:
                # Calcul du nombre total à garder, mais cette fois en fonction du nouveau nombre d'exemples négatifs !
                total_to_keep = int(round( num_negative_relations_to_keep/(1-(1-neg_pairs_percentage)) )) # 1-(1-neg_pairs_percentage) == neg_pairs_percentage
                # Puis recalcul du nombre de positifs à garder, pour conserver la proportion "neg_pairs_percentage"
                num_positive_relations_to_keep = int(round((1-neg_pairs_percentage)*total_to_keep))
                _positive_pairs = set(random.sample(list(_positive_pairs), num_positive_relations_to_keep))
        
        # On ne garde que 'num_negative_relations_to_keep' parmi les relations négatives trouvées
        _negative_pairs = set(random.sample(list(_negative_pairs), num_negative_relations_to_keep))
        
        return (_negative_pairs, _positive_pairs);
        
    
    def __generate_croc_pos_neg_pairs(self, coreference_chains, sorted_mentions_dict, nb_mentions_between=2):
        """ Méthode __generate_croc_pos_neg_pairs.
        
        Génère un ensemble d'exemples positifs et négatifs de paires de coréférences à la manière du système CROC de
        Adèle Désoyer.
        
        Les exemples positifs sont générés à partir de l'ensemble des chaînes de coréférence ordonnées 'coreference_chains'.
        Ils correspondent à l'ensemble des paires de mentions consécutives dans la chaîne de coréférence
        Les paires négatives sont seulement un nombre fixe (2 par défaut, pour obtenir le meilleur jeu de données *medium_training_set* d'Adèle)
        de paires \[$x_i$, $m_2$\] à partir d'une paire coréférente \[$m_1$, $m_2$\].
        
        Parameters
        ----------
        coreference_chains : dict
            L'ensemble des chaînes de coréférence ordonnées, à partir desquelles calculer les paires.
        sorted_mentions_dict : dict
            Le dictionnaire ordonné décrivant chacune des mentions à partir desquelles calculer les paires.
        nb_mentions_between : int
            Nombre de mentions à selectionner (i.e. nombre de paires négatives à générer) entre deux mentions d'une paire positive.
            Vaut 2, par défaut.
        
        Returns
        -------
        (set[tuple(str, str, int, int)], set[tuple(str, str, int, int)])
            Le couple `(negative_pairs, positive_pairs)` des ensembles paires positives et négatives respectivement.
            Chaque paire est décrites par le nom de la mention gauche, le nom de la mention droite,
            la valeur 1 si la paire est coréférente, 0 sinon, et le numéro de leur chaîne si coréférente, -1 sinon.
            Exemple : ('u-MENTION-jmuzerelle_1354180728605', 'u-MENTION-jmuzerelle_1354180965453', 1, 5)
        """
        # Paires résultantes avec l'étiquette associé et l'id de chaine
        positive_pairs = set()
        negative_pairs = set()
        
        # -- Construction des paires positives
        for chain in coreference_chains.keys():
            # Seulement les paires consécutives.
            for chain_mention_id in range(len(coreference_chains[chain])-1):
                positive_pairs.add((coreference_chains[chain][chain_mention_id], coreference_chains[chain][chain_mention_id+1], 1, chain))
        
        # -- Construction des paires négatives
        for m1_name, m2_name, _, _ in positive_pairs:
            ordered_mentions_names = list(sorted_mentions_dict.keys())
            m1_index = ordered_mentions_names.index(m1_name)
            m2_index = ordered_mentions_names.index(m2_name)#, m1_index) #On commence à chercher à partir de l'index m1_index pour gagner en complexité.
            
            # Mentions STRICTEMENT entre m1 et m2
            ordered_mentions_between = ordered_mentions_names[m1_index+1:m2_index]
            _nb_mentions_between = len(ordered_mentions_between) if (nb_mentions_between > len(ordered_mentions_between)) else nb_mentions_between
            randomly_selected_between_mentions = random.sample(ordered_mentions_between, _nb_mentions_between)
            
            for mi in randomly_selected_between_mentions:
                negative_pairs.add((mi, m2_name, 0, -1))
                
        return (negative_pairs, positive_pairs);
        
    
    
    def __generate_original_pos_neg_pairs(self, coreference_chains, sorted_mentions_dict, with_combinations=False):
        """ Méthode __generate_original_pos_neg_pairs.
        
        Génère un ensemble d'exemples positifs et négatifs de paires de coréférences.
        
        Les exemples positifs sont générés à partir de l'ensemble des chaînes de coréférence ordonnées 'coreference_chains'.
        Ils correspondent à l'ensemble des paires de mentions consécutives dans la chaîne de coréférence
        Les exemples négatifs sont générés par soustraction de l'ensemble des paires positives à l'ensemble des combinaisons 
        de 2 éléments parmi toutes les mentions 'sorted_mentions_dict'.
        
        Parameters
        ----------
        coreference_chains : dict
            L'ensemble des chaînes de coréférence ordonnées, à partir desquelles calculer les paires.
        sorted_mentions_dict : dict
            Le dictionnaire ordonné décrivant chacune des mentions à partir desquelles calculer les paires.
        with_combinations : bool
            Si vrai, les paires positives à générer sont toutes les combinaisons de taille 2 de chaque
            chaîne de coréférence de 'coreference_chains'.
            Sinon, les paires positives sont toutes les paires successives de chaque chaîne de coréférence de 'coreference_chains'.
            Vaut False, par défaut.
        
        Returns
        -------
        (set[tuple(str, str, int, int)], set[tuple(str, str, int, int)])
            Le couple `(negative_pairs, positive_pairs)` des ensembles paires positives et négatives respectivement.
            Chaque paire est décrites par le nom de la mention gauche, le nom de la mention droite,
            la valeur 1 si la paire est coréférente, 0 sinon, et le numéro de leur chaîne si coréférente, -1 sinon.
            Exemple : ('u-MENTION-jmuzerelle_1354180728605', 'u-MENTION-jmuzerelle_1354180965453', 1, 5)
        """
        
        # Paires résultantes avec l'étiquette associé et l'id de chaine
        positive_pairs = set()
        negative_pairs = set()
        
        # Paires positives sans étiquette ni id de chaine, pour la construction des négatifs
        _p_pairs = set()
        
        # -- Construction de toutes les paires de mentions positives possibles.
        
        # On prend chaque chaîne de coréférence, et on construit toutes les combinaisons
        # à partir de la chaîne. => Toutes les paires générées sont positives.
        for chain in coreference_chains.keys():
            if not with_combinations:
                # Seulement les paires consécutives.
                for chain_mention_id in range(len(coreference_chains[chain])-1):
                    _p_pairs.add((coreference_chains[chain][chain_mention_id], coreference_chains[chain][chain_mention_id+1]))
                    positive_pairs.add((coreference_chains[chain][chain_mention_id], coreference_chains[chain][chain_mention_id+1], 1, chain))
            else:
                # Tous les combinaisons de paires possibles.
                all_chain_pairs_combinations = itertools.combinations(reversed(list(coreference_chains[chain])), 2)
                for pair in all_chain_pairs_combinations:
                    _p_pairs.add(pair)
                    positive_pairs.add(pair+(1, chain))
        
        
        negative_pairs = set(
                             map(lambda pair : (pair[0], pair[1], 0, -1),
                                 filter(lambda pair: ((pair[1] != pair[0]) and ((pair[0], pair[1]) not in _p_pairs) and
                                                                        ((pair[1], pair[0]) not in _p_pairs)),
                                        itertools.combinations(reversed(list(sorted_mentions_dict.keys()), 2))
                                 )
                             )
                         )
        
        return (negative_pairs, positive_pairs);
        
        
    
    def __generate_weighted_original_pos_neg_pairs(self, coreference_chains, sorted_mentions_dict, neg_pairs_percentage=0.5, with_combinations=False):
        """ Méthode __generate_weighted_original_pos_neg_pairs.
        
        Génère un ensemble d'exemples positifs et négatifs de paires de coréférences.
        
        Les exemples positifs sont générés de la même manière que `__generate_original_pos_neg_pairs`.
        Les exemples négatifs sont les paires générées par `__generate_original_pos_neg_pairs`
        sous-échantillonnées aléatoirement afin de respecter la proportion `neg_pairs_percentage`
        en fonction du nombre d'exemples positifs.
        
        Parameters
        ----------
        coreference_chains : dict
            L'ensemble des chaînes de coréférence ordonnées à partir desquelles calculer les paires.
        sorted_mentions_dict : dict
            Le dictionnaire ordonné décrivant chacune des mentions à partir desquelles calculer les paires.
        neg_pairs_percentage : float
            Pourcentage d'exemples de paires négatifs à générer. Vaut 0.5 par défaut.
        with_combinations : bool
            Détermine si les paires positives à générer sont tous les combinaisons de taille 2 de chaque
            chaîne de coréférence de 'coreference_chains'.
            Sinon, les paires positives sont toutes les paires successives de chaque chaîne de coréférence de 'coreference_chains'.
            Vaut False, par défaut.
        
        Returns
        -------
        (set[tuple(str, str, int, int)], set[tuple(str, str, int, int)])
            Le couple `(negative_pairs, positive_pairs)` des ensembles paires positives et négatives respectivement.
            Chaque paire est décrites par le nom de la mention gauche, le nom de la mention droite,
            la valeur 1 si la paire est coréférente, 0 sinon, et le numéro de leur chaîne si coréférente, -1 sinon.
            Exemple : ('u-MENTION-jmuzerelle_1354180728605', 'u-MENTION-jmuzerelle_1354180965453', 1, 5)
        """
        # Calcul des paires en mode 'original'.
        negative_pairs, positive_pairs = self.__generate_original_pos_neg_pairs(coreference_chains, sorted_mentions_dict, with_combinations)
        # Equilibrage
        w_negative_pairs, w_positive_pairs = ANCORDataframeBuilder.__weight(negative_pairs, positive_pairs, neg_pairs_percentage)
        
        return (w_negative_pairs, w_positive_pairs);
    
    
    
    def __generate_window_pos_neg_pairs(self, coreference_chains, sorted_mentions_dict, window_size=30):
        """ Méthode __generate_window_pos_neg_pairs.
        
        Génère un ensemble d'exemples positifs et négatifs de paires de coréférences.
        
        Les paires positives et négatives sont calculées en associant chaque mention du dictionnaire 'json_mentions'
        à chacune des 'window_size' mentions présentes avant elle, triées par ordre d'apparition dans le texte.
        
        Parameters
        ----------
        coreference_chains : dict
            L'ensemble des chaînes de coréférence ordonnées à partir desquelles calculer les paires.
        sorted_mentions_dict : dict
            Le dictionnaire ordonné décrivant chacune des mentions à partir desquelles calculer les paires.
        window_size : int
            Taille de la fenêtre des mentions antécédantes à selectionner pour la construction des paires.
            Vaut 30 par défaut.
        
        Returns
        -------
        (set[tuple(str, str, int, int)], set[tuple(str, str, int, int)])
            Le couple `(negative_pairs, positive_pairs)` des ensembles paires positives et négatives respectivement.
            Chaque paire est décrites par le nom de la mention gauche, le nom de la mention droite,
            la valeur 1 si la paire est coréférente, 0 sinon, et le numéro de leur chaîne si coréférente, -1 sinon.
            Exemple : ('u-MENTION-jmuzerelle_1354180728605', 'u-MENTION-jmuzerelle_1354180965453', 1, 5)
        """
        
        positive_pairs = set()
        negative_pairs = set()
        
        # -- Parcours de toutes les mentions triées
        sorted_mentions_names = list(sorted_mentions_dict.keys())
        for index_cur_m, cur_m in enumerate(sorted_mentions_names):
            # Calcul de l'index butoire de la fin de la fenetre
            window_bound = index_cur_m-window_size
            if (window_bound) < 0:
                window_bound = 0

            # Construction de toutes les paires possibles entre la mention courante
            # et les window_size mentions avant elle dans l'ordre décroissant
            for index_left in range(index_cur_m, window_bound, -1):
                left_m = sorted_mentions_names[index_left-1]

                # Vérification que la paire crée est coréférente, ou non.
                is_coref = False
                for chain_id in coreference_chains.keys():
                    if (left_m in coreference_chains[chain_id]) and (cur_m in coreference_chains[chain_id]):
                        is_coref = True
                        positive_pairs.add((left_m, cur_m, 1, chain_id))
                        break;
                if not is_coref:
                    negative_pairs.add((left_m, cur_m, 0, -1))
                    
        return (negative_pairs, positive_pairs);
    
    
    
    def __generate_weighted_window_pos_neg_pairs(self, coreference_chains, sorted_mentions_dict, window_size=30, neg_pairs_percentage=0.5):
        """ Méthode __generate_weighted_window_pos_neg_pairs.
        
        Génère un ensemble d'exemples positifs et négatifs de paires de coréférences.
        
        Les paires positives et négatives sont calculées en associant chaque mention du dictionnaire 'json_mentions'
        à chacune des 'window_size' mentions présentes avant elle, triées par ordre d'apparition dans le texte.
        Contrairement à __generate_window_pos_neg_pairs, on conserve la proportion 'neg_pairs_percentage' d'exemples
        négatifs en fonction du nombre de paires positives générées.
        
        Parameters
        ----------
        coreference_chains : dict
            L'ensemble des chaînes de coréférence ordonnées à partir desquelles calculer les paires.
        sorted_mentions_dict : dict
            Le dictionnaire ordonné décrivant chacune des mentions à partir desquelles calculer les paires.
        window_size : int
            Taille de la fenêtre des mentions antécédantes à selectionner pour la construction des paires.
            Vaut 30 par défaut.
        neg_pairs_percentage : float
            Pourcentage d'exemples de paires négatifs à générer. Vaut 0.5 par défaut.
        
        Returns
        -------
        (set[tuple(str, str, int, int)], set[tuple(str, str, int, int)])
            Le couple `(negative_pairs, positive_pairs)` des ensembles paires positives et négatives respectivement.
            Chaque paire est décrites par le nom de la mention gauche, le nom de la mention droite,
            la valeur 1 si la paire est coréférente, 0 sinon, et le numéro de leur chaîne si coréférente, -1 sinon.
            Exemple : ('u-MENTION-jmuzerelle_1354180728605', 'u-MENTION-jmuzerelle_1354180965453', 1, 5)
        """
        # Calcul des paires en mode 'original'.
        negative_pairs, positive_pairs = self.__generate_window_pos_neg_pairs(coreference_chains, sorted_mentions_dict, window_size)
        
        # Equilibrage
        w_negative_pairs, w_positive_pairs = ANCORDataframeBuilder.__weight(negative_pairs, positive_pairs, neg_pairs_percentage)
        
        return (w_negative_pairs, w_positive_pairs);
            
        
    
    def __generate_locally_weighted_window_pos_neg_pairs(self, coreference_chains, sorted_mentions_dict, window_size=30, neg_pairs_percentage=0.5, absolute=True):
        """ Méthode __generate_weighted_window_pos_neg_pairs.
        
        Génère un ensemble d'exemples positifs et négatifs de paires de coréférences.
        
        .. deprecated:: 0.3.5
            La pondération est effectuée sur chaque fenêtre. et pose deux risques pour chaque fenêtre :
            1. Si une fenêtre contient une majorité de paires positives : omission de certaines paires positives pour respecter l'équilibrage absolu dans le cas `absolute`.
            2. Si une fenêtre ne contient aucune paire positive, on omettra l'entièreté des paires associées à cette fenêtre, ce qui implique un problème de repartition
                des données.
            Utilisez :func:`__generate_weighted_window_pos_neg_pairs` à la place, qui évite ces problèmes en effectuant son équilibrage sur l'entièreté des données.
            Par construction, il y aura une plus grande proportion de paires négatives sur l'entièreté des données (risque 1. écarté), et les exemples négatifs à exclure
            seront selectionnés sur l'ensemble des données, en évitant d'omettre des fenêtres entières (risque 2. écarté).
        
        Les paires positives et négatives sont calculées en associant chaque mention du dictionnaire 'json_mentions'
        à chacune des 'window_size' mentions présentes avant elle, triées par ordre d'apparition dans le texte.
        Les paires ainsi générées dans une fenêtre sont équilibrées pour atteindre un taux `neg_pairs_percentage`
        de paires négatives.
        
        Parameters
        ----------
        coreference_chains : dict
            L'ensemble des chaînes de coréférence ordonnées à partir desquelles calculer les paires.
        sorted_mentions_dict : dict
            Le dictionnaire ordonné décrivant chacune des mentions à partir desquelles calculer les paires.
        window_size : int
            Taille de la fenêtre des mentions antécédantes à selectionner pour la construction des paires.
            Vaut 30 par défaut.
        neg_pairs_percentage : float
            Pourcentage d'exemples de paires négatifs à générer. Vaut 0.5 par défaut.
        absolute : bool
            Détermine si la proportion dans chaque fenêtre doit être conservée de manière absolue ou non.
            Dans ce cas, s'il y a plus d'exemples positifs que d'exemples négatifs dans une fenêtre,
            le taux d'exemples positifs est également recalculé.
            Vaut False par défaut.
        
        Returns
        -------
        (set[tuple(str, str, int, int)], set[tuple(str, str, int, int)])
            Le couple `(negative_pairs, positive_pairs)` des ensembles paires positives et négatives respectivement.
            Chaque paire est décrites par le nom de la mention gauche, le nom de la mention droite,
            la valeur 1 si la paire est coréférente, 0 sinon, et le numéro de leur chaîne si coréférente, -1 sinon.
            Exemple : ('u-MENTION-jmuzerelle_1354180728605', 'u-MENTION-jmuzerelle_1354180965453', 1, 5)
        """
        
        positive_pairs = set()
        negative_pairs = set()
        
        # -- Parcours de toutes les mentions triées
        sorted_mentions_names = list(sorted_mentions_dict.keys())
        for index_cur_m, cur_m in enumerate(sorted_mentions_names):
            pos_pairs_cur_m = set()
            neg_pairs_cur_m = set()
            # Calcul de l'index butoire de la fin de la fenetre
            window_bound = index_cur_m-window_size if index_cur_m-window_size>=0 else 0
                
            # -- Construction de toutes les paires possibles entre la mention courante
            # -- et les window_size mentions avant elle dans l'ordre décroissant
            for index_left in range(index_cur_m, window_bound, -1):
                left_m = sorted_mentions_names[index_left]
                # Vérification que la paire crée est coréférente, ou non.
                is_coref = False
                for chain_id in coreference_chains.keys():
                    if (left_m in coreference_chains[chain_id]) and (cur_m in coreference_chains[chain_id]):
                        is_coref = True
                        pos_pairs_cur_m.add((left_m, cur_m, 1, chain_id))
                        break;
                if not is_coref:
                    neg_pairs_cur_m.add((left_m, cur_m, 0, -1))
            
            # -- Equilibrage des paires dans la 
            w_neg_pairs_cur_m, w_pos_pairs_cur_m = ANCORDataframeBuilder.__weight(neg_pairs_cur_m, pos_pairs_cur_m, neg_pairs_percentage, absolute)
            
            # -- Ajout des paires trouvées dans la mention courante
            positive_pairs.update(w_pos_pairs_cur_m)
            negative_pairs.update(w_neg_pairs_cur_m)
        
        return (negative_pairs, positive_pairs);
    
    
    
    def __generate_dataframe(self, pairs_set, sorted_mentions_dict, file_path, start_id=0, verbose=True):
        """ Méthode __generate_training_test_dataset.
        
        Génère et retourne un dataframe contenant les exemples positifs et négatifs de paires de coréférences de 'pairs_set'
        avec leur ensemble de valeurs de traits.
        
        Parameters
        ----------
        pairs_set : set[tuple(str, str, int, int)]
            Un ensemble de paires de mentions, décrites par le nom de la mention gauche, le nom de la mention droite,
            la valeur 1 si la paire est coréférente, 0 sinon, et le numéro de leur chaîne si coréférente, -1 sinon.
            Exemple : ('u-MENTION-jmuzerelle_1354180728605', 'u-MENTION-jmuzerelle_1354180965453', 1, 5)
            
        sorted_mentions_dict : dict
            Le dictionnaire ordonné décrivant l'ensemble des mentions contenues dans les paires avec leurs traits,
            d'après les annotations du corpus ANCOR.
            
        file_path : str
            Chemin du fichier de corpus ANCOR duquel provient tous les exemples du dataframe à créer.
        
        start_id : int
            Définit à quel nombre commencer la numérotation des paires du dictionnaire (pour pouvoir fusionner les dictionnaires de chaque fichier ensuite dans build).
        
        verbose : bool
            Mode verbeux. Vaut 'True' par défaut.
        
        Returns
        -------
        dict[dict]
            Le dataframe sous forme de dictionnaire contenant tous les exemples positifs et négatifs de paires de coréférences de 'pairs_set'
            avec leur ensemble de valeurs de traits.
            Le dictionnaire est orienté au format 'index' de pandas. A chaque clé est associée une ligne décrite par un dictionnaire nom_de_colonne:valeur
            Exemple : {"0": {"Chain_Id":"5", ...},
                       "1": {"Chain_Id":"5", ...},
                       ...
                      }
        """
        
        # Fonction de calcul du nombre de tokens en commun
        def compute_shared_tokens(small, long):
            _small = list(small)
            _long = list(long)
            shared = []
            for token in _small:
                if token in _long:
                    shared.append(token)
                    _long.remove(token) # retire la première occurence
            return shared;
        
        
        # Dictionnaire contenant tout le dataframe des données du fichier courant
        dataframe_dict = {}
        
        # Lecteur de fichier pour accéder à des valeurs intermédiaires
        file_reader = XMLTEIFileReader(file_path)
        
        # Convertisseur de True/False en "YES"/"NO"
        tf2yn = {True: "YES", False: "NO"}
        
        # Parcours de toutes les paires à placer dans le dataframe
        pair_id = start_id
        
        print(f"{len(pairs_set)} paires à générer")
        for pair in pairs_set:
            #print((f"Paire numéro {pair_id} en cours")
        
            #######################################################################################################
            #                                                                                                     #
            #                                                                                                     #
            #                             Récupération des mentions de la paire                                   #
            #                                                                                                     #
            #                                                                                                     #
            #######################################################################################################
            
            LEFT_NAME = pair[0]
            RIGHT_NAME = pair[1]
            
            # On arrête tout si l'identifiant de la mention gauche ou droite n'existe pas
            if ((LEFT_NAME not in sorted_mentions_dict.keys()) or (RIGHT_NAME not in sorted_mentions_dict.keys())):
                continue;
            
            # Inversion des mentions gauche et droite, si left_index > right_index
            if (TeiId.compare_tei_id(TeiId(sorted_mentions_dict[LEFT_NAME]["START_ID"]),
                                     TeiId(sorted_mentions_dict[RIGHT_NAME]["START_ID"])) > 0):
                LEFT_NAME = pair[1]
                RIGHT_NAME = pair[0]
                
            # Récupération des mentions gauche et droite en JSON avec leurs traits
            left_unit = sorted_mentions_dict[LEFT_NAME]
            right_unit = sorted_mentions_dict[RIGHT_NAME]

            # Récupération des index des mots de départ des mentions gauche et droite
            left_index = TeiId(left_unit["START_ID"])
            right_index = TeiId(right_unit["START_ID"])
                
            structural_feat_dict = {}
            structural_feat_dict["LEFT_NAME"] = LEFT_NAME  # ID de mention gauche
            structural_feat_dict["RIGHT_NAME"] = RIGHT_NAME  # ID de mention droite
            #structural_feat_dict["Sub_Corpus"] = os.path.dirname(file_path) # Sous-corpus de la paire
            structural_feat_dict["FILE_NAME"] = os.path.basename(file_path) # Fichier de la paire
            structural_feat_dict["CHAIN_ID"] = pair[3] # Identifiant de la chaîne de coréf dans laquelle est la mention


            #######################################################################################################
            #                                                                                                     #
            #                                                                                                     #
            #                                           TRAITS DE MENTION                                         #
            #                                                                                                     #
            #                                                                                                     #
            #######################################################################################################
            
            m1_feat_dict = {}
            m2_feat_dict = {}
            
            
            # -- Calcul du CONTENT
            m1_feat_dict[f'M1_CONTENT'] = (str(left_unit["CONTENT"]) if (("CONTENT" in left_unit) and (left_unit["CONTENT"] is not None) and (isinstance(left_unit["CONTENT"], str)) and (len(left_unit["CONTENT"]) > 0))
                                            else " ".join(file_reader.get_words_in(left_index, TeiId(left_unit["END_ID"]), inclusive=True)))
            m2_feat_dict[f'M2_CONTENT'] = (str(right_unit["CONTENT"]) if (("CONTENT" in left_unit) and (left_unit["CONTENT"] is not None) and (isinstance(left_unit["CONTENT"], str)) and (len(left_unit["CONTENT"]) > 0))
                                            else " ".join(file_reader.get_words_in(right_index, TeiId(right_unit["END_ID"]), inclusive=True)))
            m1_feat_dict[f'M1_CONTENT'] = " ".join(m1_feat_dict[f'M1_CONTENT'].strip().split())
            m2_feat_dict[f'M2_CONTENT'] = " ".join(m2_feat_dict[f'M2_CONTENT'].strip().split())
            
            # On arrête tout si le contenu des mention est vide ou erroné
            if ( ((len(m1_feat_dict["M1_CONTENT"]) <= 0) or (m1_feat_dict["M1_CONTENT"] is None)) or
                    ((len(m2_feat_dict["M2_CONTENT"]) <= 0) or (m2_feat_dict["M2_CONTENT"] is None)) ):
                continue;
            
            
            
            # -- Calcul des traits de mention
            #print(("    Calcul des traits de mentions...", end='')
            feat_in_mentions_list = ["CONTENT", "TYPE", "GP", "GENRE", "NB", "EN"]
            for feat in feat_in_mentions_list:
                if feat != "CONTENT":
                    m1_feat_dict[f'M1_{feat}'] = left_unit[feat] if feat in left_unit else None
                    m2_feat_dict[f'M2_{feat}'] = right_unit[feat] if feat in right_unit else None
            #print((" OK")
            
            
            # --- Calcul des distances en mentions, mots et en caractères
            # print(("    Calcul des distances...", end='')
            # Rappel : le slicing est inclusif sur l'index de début, et exclusif sur l'index de fin
            # Mehdi regardait donc left_unit["END_ID"] <= w_id < right_unit["START_ID"]
            # string_text = self.data_source[left_unit["END_ID"]:right_unit["START_ID"]]
            # Dans notre cas, on décide d'être exclusif totalement.
            words_list_between = file_reader.get_words_in(TeiId(left_unit["END_ID"]), TeiId(right_unit["START_ID"]), inclusive=False)
            string_text_between = ' '.join(words_list_between)
            # On veut compter "l'oeuf" comme deux mots.
            words_list_between = re.sub("\'", " ", string_text_between).strip().split()

            # Calcul du nombre de caractères entre les mentions
            dist_chars_acc = len(string_text_between)
            #print((" OK")



            #######################################################################################################
            #                                                                                                     #
            #                                                                                                     #
            #                                           TRAITS RELATIONNELS                                       #
            #                                                                                                     #
            #                                                                                                     #
            #######################################################################################################
            
            relationnal_feat_dict = {}
            
            m1 = m1_feat_dict[f'M1_CONTENT']
            m2 = m2_feat_dict[f'M2_CONTENT']
            small = min([m1, m2], key=lambda m: len(m))
            long = max([m1, m2], key=lambda m: len(m))
            # TODO: Remove this part
            if (len(small)<=0) or (len(long)<=0):
            	print("\nsmall =", small)
            	print("long =", long)
            	print("m1 =", m1)
            	print("m2 =", m2, "\n")
            small_tokenized = re.sub("\'", " ", small).strip().split()
            if len(small_tokenized) <= 0:
                small_tokenized = small.strip().split()
            long_tokenized = re.sub("\'", " ", long).strip().split()
            if len(long_tokenized) <= 0:
                long_tokenized = long.strip().split()
            
            
            
            # -- Traits d'inclusion
            
            # Forme complete identique en caractères
            relationnal_feat_dict["ID_FORM"] = tf2yn[m1 == m2]
            
            # Si small est une sous-forme de long (i.e. small incluse strictement dans long)
            # e.g. "Bonjour" est incluse dans "Bonjour à tous les amis"
            #      "Bonjour les amis" n'est PAS incluse dans "Bonjour à tous les amis"
            relationnal_feat_dict["ID_SUBFORM"] = tf2yn[small in long] 
            
            # Taux d'inclusion des tokens
            # (on n'utilise pas set.intersection pour pouvoir calculer le taux 
            # sur des mentions qui contiennent plusieurs fois le meme token)
            # Exemple : long : 'L homme n est pas un loup pour l homme'
            #           small : 'L homme est un loup loup pour l homme'
            #           On voit bien dans ce cas que small (en terme de nb tokens)
            #           n'est pas entièrement inclue dans long, car long ne contient
            #           qu'un seul token "loup".
            len_small = len(small)
            relationnal_feat_dict["INCL_RATE"] = len(compute_shared_tokens(small_tokenized, long_tokenized))/len(small) if len_small > 0 else 0.
            # Taux de tokens communs
            small_tokenized_set = set(small_tokenized)
            long_tokenized_set = set(long_tokenized)
            len_union = len(small_tokenized_set.union(long_tokenized_set))
            relationnal_feat_dict["COM_RATE"] = len(small_tokenized_set.intersection(long_tokenized_set))/len_union if len_union > 0 else 0.
            
            
            
            # -- Traits d'identité
            
            # Identicité du type d'EN
            relationnal_feat_dict["ID_EN"] = tf2yn[(m1_feat_dict[f'M1_EN'] == m2_feat_dict[f'M2_EN'])] 
            
            # Identicité du GP
            relationnal_feat_dict["ID_GP"] = tf2yn[(m1_feat_dict[f'M1_GP'] == m2_feat_dict[f'M2_GP'])]
            
            # Identicité du genre
            if (m1_feat_dict[f'M1_GENRE'] == "UNK") or (m2_feat_dict[f'M2_GENRE'] == "UNK"):
                # Si un des deux ou les deux sont UNK, indetermine
                relationnal_feat_dict["ID_GENRE"] = "UNK"
            else:
                # Cas MASC/FEM/NULL
                relationnal_feat_dict["ID_GENRE"] = tf2yn[(m1_feat_dict[f'M1_GENRE'] == m2_feat_dict[f'M2_GENRE'])]
            
            # Identicité du nombre
            if (m1_feat_dict[f'M1_NB'] == "UNK") or (m2_feat_dict[f'M2_NB'] == "UNK"):
                # Si un des deux ou les deux sont UNK, indetermine
                relationnal_feat_dict["ID_NB"] = "UNK"
            else:
                # Cas SING/PLUR
                relationnal_feat_dict["ID_NB"] = tf2yn[(m1_feat_dict[f'M1_NB'] == m2_feat_dict[f'M2_NB'])]
            
            # Identicité du previous
            m1_prev = left_unit["PREVIOUS"] if "PREVIOUS" in left_unit else "UNK"
            m2_prev = right_unit["PREVIOUS"] if "PREVIOUS" in right_unit else "UNK"
            if (m1_prev == "UNK") or (m2_prev == "UNK"):
                # Si un des deux ou les deux sont UNK, indetermine
                relationnal_feat_dict["ID_PREV"] = "UNK"
            else:
                # Cas général
                relationnal_feat_dict["ID_PREV"] = tf2yn[(m1_prev == m2_prev)] 
            
            # Identicité du next
            m1_next = left_unit["NEXT"] if "NEXT" in left_unit else "UNK"
            m2_next = right_unit["NEXT"] if "NEXT" in right_unit else "UNK"
            if (m1_next == "UNK") or (m2_next == "UNK"):
                # Si un des deux ou les deux sont UNK, indetermine
                relationnal_feat_dict["ID_NEXT"] = "UNK"
            else:
                # Cas général
                relationnal_feat_dict["ID_NEXT"] = tf2yn[(m1_next == m2_next)]
                                                     
            # Identicité du speaker
            m1_spk = left_unit["SPEAKER"] if "SPEAKER" in left_unit else "UNK"
            m2_spk = right_unit["SPEAKER"] if "SPEAKER" in right_unit else "UNK"
            if (m1_spk == "UNK") or (m2_spk == "UNK"):
                # Si un des deux ou les deux sont UNK, indetermine
                relationnal_feat_dict["ID_SPK"] = "UNK"
            else:
                # Cas général
                relationnal_feat_dict["ID_SPK"] = tf2yn[(m1_spk == m2_spk)]
            
            
            # Enchâssement de m2 dans m1
            # Riche car si vrai, m2 apporte des informations complémentaires
            # à m1, mais SIGNIFIE que les deux ne pourront jamais référer à une même entité.
            m1_content = m1_feat_dict[f'M1_CONTENT']
            m2_content = m2_feat_dict[f'M2_CONTENT']
            left_unit = sorted_mentions_dict[LEFT_NAME]
            right_unit = sorted_mentions_dict[RIGHT_NAME]
            small_mention = min([(m1_content, left_unit), (m2_content, right_unit)], key=lambda elt: len(elt[0]))[1]
            long_mention = max([(m1_content, left_unit), (m2_content, right_unit)], key=lambda elt: len(elt[0]))[1]
            small_mention_start = TeiId(small_mention["START_ID"])
            small_mention_end = TeiId(small_mention["END_ID"])
            long_mention_start = TeiId(long_mention["START_ID"])
            long_mention_end = TeiId(long_mention["END_ID"])
            
            # si small occupe [a, b] et long [A, B], alors small est enchâssée dans long ssi
            # A <= a <= B ET A <= b <= B
            embedded = (((long_mention_start.compare(small_mention_start) <= 0) and (small_mention_start.compare(long_mention_end) <= 0)) and
                        ((long_mention_start.compare(small_mention_end)   <= 0) and (small_mention_end.compare(long_mention_end)   <= 0)))
            relationnal_feat_dict["EMBEDDED"] = tf2yn[embedded]
            
            
            # -- Traits de distance
            
            # Nombre de mentions entre les deux mentions
            relationnal_feat_dict["DISTANCE_MENTION"] = (int(right_unit["NUM"]) - int(left_unit["NUM"]))-1 # -1 car on veut le nombre de mentions ENTRE les deux
            # Distance en tours de paroles
            relationnal_feat_dict["DISTANCE_TURN"] = TeiId(right_unit["START_ID"]).u - TeiId(left_unit["START_ID"]).u
            # Nombre de mots entre les mentions
            relationnal_feat_dict["DISTANCE_WORD"] = len(words_list_between) # Nombre de mots ENTRE les mentions
            # Nombre de caractères (espaces comprises) ENTRE les mentions
            relationnal_feat_dict["DISTANCE_CHAR"] = len(string_text_between) 
            
            
            # Coreference de la paire
            relationnal_feat_dict["IS_CO_REF"] = pair[2]
            #print((" OK")
            
            
            #######################################################################################################
            #                                                                                                     #
            #                                                                                                     #
            #                                Ajout de la paire avec ses attributs                                 #
            #                                                                                                     #
            #                                                                                                     #
            #######################################################################################################
            
            full_row_dict = {**structural_feat_dict, **m1_feat_dict, **m2_feat_dict, **relationnal_feat_dict}
            dataframe_dict[pair_id] = full_row_dict
            #print(" OK")
            if verbose:
            	print(f"\rPaire numéro {pair_id} finie", end="")
            pair_id += 1
        
        if verbose:
            print()
            
        return dataframe_dict;
    
    
    
    def __recompute(self, sorted_mentions_dict, file_reader):
        recomp_sorted_mentions_dict = {}
        # Parcours des mentions
        for num, mention_name in enumerate(list(sorted_mentions_dict.keys()), 1):
            # Copie des traits de la mention courante pour pouvoir les modifier
            mention_feat_dict = dict(sorted_mentions_dict[mention_name])
            
            # -- Verification du contenu textuel de la mention + recalcul éventuel
            if (("CONTENT" not in mention_feat_dict) or (len(mention_feat_dict["CONTENT"]) <= 0) or (mention_feat_dict["CONTENT"] is None)):
                mention_feat_dict["CONTENT"] = " ".join(file_reader.get_words_in(TeiId(mention_feat_dict["START_ID"]), TeiId(mention_feat_dict["END_ID"]), inclusive=True))
            
            # -- Modification des traits
            mention_feat_dict["CONTENT"] = " ".join(mention_feat_dict["CONTENT"].strip().split())
            
            # On arrête tout si le contenu de la mention est vide ou erroné
            if (("CONTENT" not in mention_feat_dict) or (len(mention_feat_dict["CONTENT"]) <= 0) or (mention_feat_dict["CONTENT"] is None)):
                continue;
            
            mention_feat_dict['START_ID'] = mention_feat_dict.pop('START_ID')
            mention_feat_dict['END_ID'] = mention_feat_dict.pop('END_ID')
            
            mention_feat_dict['NUM'] = num
            
            
            # -- Parsing de la mention
            m1_spacy = self.spacy_model(mention_feat_dict["CONTENT"])
            
            # -- Entitée nommée
            mention_feat_dict['EN'] = "NO"
            if m1_spacy.ents:
                e = next(iter(m1_spacy.ents))
                if ( (e.start_char==0) and (e.end_char==len(mention_feat_dict["CONTENT"])) ):
                    mention_feat_dict['EN'] = e.label_
            
            
            # -- Calcul de POS, GENRE, et NB
            # Groupement du contenu de la mention comme un unique token
            with m1_spacy.retokenize() as retokenizer_m1:
                retokenizer_m1.merge(m1_spacy[0:len(m1_spacy)])
            token = next(iter(m1_spacy))
            mention_feat_dict['TYPE'] = token.pos_ if token.pos_ else "UNK"
            
            # En français, seulement Masc/Fem et Sing/Plur
            # On teste si la valeur est déjà annotée comme NULL (artefact
            # lié à certaines disfluences) parce que le modèle ne pourra pas
            # l'analyser.
            m1_morph = token.morph.to_dict()
            
            if (('GENRE' not in mention_feat_dict) or 
                ('GENRE' in mention_feat_dict and (mention_feat_dict['GENRE'] != "NULL"))):
                mention_feat_dict['GENRE'] = m1_morph['Gender'] if 'Gender' in m1_morph else "UNK"
                
            if (('NB' not in mention_feat_dict) or 
                ('NB' in mention_feat_dict and (mention_feat_dict['NB'] != "NULL"))):
                mention_feat_dict['NB'] = m1_morph['Number'] if 'Number' in m1_morph else "UNK"
            
            
            # -- Recherche des groupes prépositionnels dans la phrase de chaque mention
            m1_sent_spacy = self.spacy_model(file_reader.get_sentence_of_mention(TeiId(mention_feat_dict["START_ID"])))
            mention_feat_dict['GP'] = "NO"
            for token_m1_sent in m1_sent_spacy:
                if (((token_m1_sent.pos_.upper() == "ADP") or (token_m1_sent.dep_.upper() == "CASE")) and
                        (token_m1_sent.head.text in mention_feat_dict["CONTENT"])):
                    mention_feat_dict['GP'] = "YES"
                    break;
            
            recomp_sorted_mentions_dict[mention_name] = mention_feat_dict
        
        return recomp_sorted_mentions_dict;


    @preconditions(
        lambda mode: mode in ["croc", "original", "weighted_original", "window", "weighted_window"],
        lambda coreference_types : set(list(map(str.upper, coreference_types))).issubset(set(["DIRECTE", "INDIRECTE", "ANAPHORE", "UNK"])),
        )
    def build(self, mode, coreference_types, recalculate=True, save_files=True, verbose=True, include_discontinuous=True, **kw_mode_args):
        """ Méthode build.
        
        Construit le dataframe complet à partir du corpus de fichier ANCOR de l'objet, et sauvegarde le dataframe au format csv dans
        un fichier.
        Le fichier est sauvegardé dans le repertoire de sauvegarde de l'objet, et est nommé selon le mode de selection 'mode' 
        et ses '**kw_mode_args' arguments associés.
        
        Parameters
        ----------
        mode : str
            Le mode de selection des paires dans l'ensemble des mentions du corpus, dans {"croc", "original", "weighted_original", "window", "weighted_window"}.
            
            "croc" : Selection des données à la manière du système CROC.
            Les exemples positifs sont générés à partir de l'ensemble des chaînes de coréférence ordonnées 'coreference_chains'.
            Ils correspondent à l'ensemble des paires de mentions consécutives dans la chaîne de coréférence
            Les paires négatives sont seulement un nombre fixe (2 par défaut, pour obtenir le meilleur jeu de données *medium_training_set* d'Adèle)
            de paires \[$x_i$, $m_2$\] à partir d'une paire coréférente \[$m_1$, $m_2$\].
            
            "original" : Selection des données originales du corpus.
            Les exemples positifs sont générés à partir de l'ensemble des chaînes de coréférence ordonnées ordonnées du corpus.
            Les exemples négatifs sont générés par construction des combinaisons négatives de 2 éléments parmi toutes les mentions du corpus.
            
            "weighted_original" : Selection aléatoire pondérée.
            Les exemples positifs sont générés à partir de l'ensemble des chaînes de coréférence ordonnées ordonnées du corpus.
            Les exemples négatifs sont générés par tirage aléatoire de paires de mentions, parmi toutes les mentions du corpus,
            jusqu'à atteindre un taux de paires négatives 'neg_pairs_percentage' donné en argument dans **kw_mode_args.
            Les exemples positifs peuvent être tous les combinaisons de taille 2 de chaque chaîne de coréférence du corpus
            si 'with_combinations==True', ou toutes les paires de mentions consécutives de chaque chaîne, sinon.
            
            "window" : Selection par fenêtre.
            Les paires positives et négatives sont calculées en associant chaque mention du corpus
            à chacune des 'window_size' mentions présentes avant elle, triées par ordre d'apparition dans le texte.
            
            "weighted_window" : Selection par fenêtre avec pondération.
            Les paires positives et négatives sont selectionnées comme avec le mode "window", mais en veillant à conserver
            une proportion 'neg_pairs_percentage'.
            
        coreference_types : list[str]
            La liste des types de coréférence à extraire du corpus ANCOR.
            
        recalculate : bool
            Détermine si les traits des paires de mentions doivent être recalculées par l'objet (modèle spaCy),
            ou prendre les valeurs d'annotation des mentions dans le corpus.
            Vaut 'True' par défaut.
            
        save_files : bool
            Détermine si le dataframe associé à chaque fichier de corpus doit être sauvegardé dans le repertoire de sortie
            `dataframe_output_folder` ou non.
            Vaut `True` par défaut.
        
        verbose : bool
            Mode verbeux. Vaut 'True' par défaut.
            
        include_discontinuous : bool
            Détermine si les mentions discontinues du fichier doivent être considérées ou non.
            Vaut `True` par défaut.
        
        Keyword Args
        ------------
        nb_mentions_between : int
            Nombre de mentions à selectionner (i.e. nombre de paires négatives à générer) entre deux mentions d'une paire positive.
            Vaut 2, par défaut.
        
        neg_pairs_percentage : float
            Pourcentage d'exemples de paires négatifs à générer.
            Vaut 0.5 par défaut.
        
        with_combinations : bool
            Détermine si les paires positives à générer sont l'union des combinaisons de 2 éléments parmi l'ensemble
            des mentions de chaque chaîne de coréférence du corpus.
            Sinon, les paires positives sont l'union des paires de mentions successives de chaque chaîne de coréférence du corpus.
            Vaut True, par défaut.
        
        window_size : int
            Taille de la fenêtre des mentions antécédantes à selectionner pour la construction des paires.
            Vaut 30 par défaut.
            
        Raises
        ------
        ValueError
            Si les arguments passés ne sont pas du bon type ou du bon format.
        """
        
        # Récupération des **kwargs
        if mode == "croc":
            if ("nb_mentions_between" in kw_mode_args):
                if isinstance(kw_mode_args["nb_mentions_between"], int) and kw_mode_args["nb_mentions_between"] >= 0:
                    nb_mentions_between = kw_mode_args["nb_mentions_between"]
                else :
                    raise ValueError("\'nb_mentions_between\' keyword argument should be a positive integer.")
            else :
                nb_mentions_between = 2
        elif mode == "original":
            if ("with_combinations" in kw_mode_args):
                if isinstance(kw_mode_args["with_combinations"], bool):
                    with_combinations = kw_mode_args["with_combinations"]
                else :
                    raise ValueError("\'with_combinations\' keyword argument should be of bool type.")
            else :
                with_combinations = False
                
        elif mode == "weighted_original":
            if ("neg_pairs_percentage" in kw_mode_args):
                if isinstance(kw_mode_args["neg_pairs_percentage"], float):
                    neg_pairs_percentage = kw_mode_args["neg_pairs_percentage"]
                else :
                    raise ValueError("\'neg_pairs_percentage\' keyword argument should be of float type.")
            else :
                neg_pairs_percentage = 0.5
            
            if ("with_combinations" in kw_mode_args):
                if isinstance(kw_mode_args["with_combinations"], bool):
                    with_combinations = kw_mode_args["with_combinations"]
                else :
                    raise ValueError("\'with_combinations\' keyword argument should be of bool type.")
            else :
                with_combinations = False
        
        elif mode == "window":
            if ("window_size" in kw_mode_args):
                if isinstance(kw_mode_args["window_size"], int):
                    window_size = kw_mode_args["window_size"]
                else :
                    raise ValueError("\'window_size\' keyword argument should be of int type.")
            else :
                window_size = 30
                
        elif mode == "weighted_window":
            if ("window_size" in kw_mode_args):
                if isinstance(kw_mode_args["window_size"], int):
                    window_size = kw_mode_args["window_size"]
                else :
                    raise ValueError("\'window_size\' keyword argument should be of int type.")
            else :
                window_size = 30
            
            if ("neg_pairs_percentage" in kw_mode_args):
                if isinstance(kw_mode_args["neg_pairs_percentage"], float):
                    neg_pairs_percentage = kw_mode_args["neg_pairs_percentage"]
                else :
                    raise ValueError("\'neg_pairs_percentage\' keyword argument should be of float type.")
            else :
                neg_pairs_percentage = 0.5
        else:
            raise ValueError("\'mode\' argument should be in [\"croc\",\"original\", \"weighted_original\", \"window\", \"weighted_window\"].")
        
        
        # -- Parcours de tous les fichiers du corpus.
        
        merged_dict = {}
        global_num_examples = 0
        for cur_file in self.ancor_corpus_files:
            # Extraction
            if verbose:
                print("Extraction de :", os.path.basename(cur_file))
            #print(("Création du parser...", end='')
            file_reader = XMLTEIFileReader(cur_file)
            #print((" OK")
            #print(("Generation des mentions...", end='')
            mentions = file_reader.get_mentions(include_discontinuous=include_discontinuous)
            #print((" OK")
            
            #print(("Generation des chaines...", end='')
            """
            if mode == "croc":
                chains = file_reader.get_chains()
            else:
                relations = file_reader.get_coreferences(coreference_types)
                chains = file_reader.compute_chains(relations)
            """
            relations = file_reader.get_coreferences(coreference_types)
            chains = file_reader.compute_chains(relations, mentions)
            #print((" OK")
            
            if len(chains) <= 0:
                continue;
            
            if recalculate:
                mentions = self.__recompute(mentions, file_reader)
            
            # Generation des paires
            #print(("Generation des paires...", end='')
            if mode == "croc":
                pos_neg_pairs = self.__generate_croc_pos_neg_pairs(chains, mentions, nb_mentions_between)
            elif mode == "original":
                pos_neg_pairs = self.__generate_original_pos_neg_pairs(chains, mentions, with_combinations)
            elif mode == "weighted_original":
                pos_neg_pairs = self.__generate_weighted_original_pos_neg_pairs(chains, mentions, neg_pairs_percentage, with_combinations)
            elif mode == "window":
                pos_neg_pairs = self.__generate_window_pos_neg_pairs(chains, mentions, window_size)
            elif mode == "weighted_window":
                pos_neg_pairs = self.__generate_weighted_window_pos_neg_pairs(chains, mentions, window_size, neg_pairs_percentage)
            else:
                raise ValueError("\'mode\' argument should be in  [\"croc\",\"original\", \"weighted_original\", \"window\", \"weighted_window\"].")
            #print((" OK")
            
            # Generation du dataframe du fichier
            #print(("Generation du dataframe...", end='')
            cur_file_df_dict = self.__generate_dataframe(pos_neg_pairs[0].union(pos_neg_pairs[1]), mentions, cur_file, global_num_examples, verbose)
            #print((" OK")
            
            merged_dict.update(cur_file_df_dict)
            
            if save_files:
                cur_file_df = pd.DataFrame.from_dict(cur_file_df_dict, orient='index')
                cur_file_df.to_csv(self.dataframe_output_folder + "/" + os.path.basename(cur_file).replace(".tei", ".csv"))
            
            global_num_examples += len(cur_file_df_dict)
            total = len(merged_dict)
            if verbose:
                print("Exemples générés : ", len(cur_file_df_dict))
                print("Nombre d'exemples total : ", total)
            print(f"Extraction de {os.path.basename(cur_file)} terminée avec succès (total : {total}).\n")
         
         
        # -- Creation du DataFrame du corpus (concaténation des dataframes de tous les fichiers du corpus)
        
        # Nommage du fichier
        file_name_attributes = {}
        file_name_attributes["DF_NAME"] = str(self.df_name)
        file_name_attributes["coref_types"] = "FULL" if (len(coreference_types) == 4) else "_".join(coreference_types).upper()
        file_name_attributes["mode"] = str(mode)
        if ("window" in mode) or ("weighted" in mode):
            file_name_attributes["mod_add"] = "" 
            file_name_attributes["mod_add"] += str(window_size) if ("window" in mode) else ""
            file_name_attributes["mod_add"] += ("_"+str(neg_pairs_percentage)) if ("weighted" in mode) else ""
            if (mode == "weighted_original"):
                file_name_attributes["mod_add"] += "_PERMUT" if with_combinations else "_CONSEC"
        file_name_attributes["recalculate"] = "RECALC" if recalculate else "ANNOTA"
        file_name_attributes["incl_discontinuous"] = "INCL-DISCONTINUOUS" if include_discontinuous else ""
        
        corpus_df_file_name = "_".join(file_name_attributes.values()).upper()+".csv"
        corpus_df_file_path = os.path.join(self.dataframe_output_folder, corpus_df_file_name)

        # Creation du fichier
        if not os.path.exists(self.dataframe_output_folder):
            try:
                os.makedirs(self.dataframe_output_folder)
            except OSError:
                print(f"\nEchec de la creation du repertoire {self.dataframe_output_folder}.")
            else:
                print(f"\nRepertoire {self.dataframe_output_folder} créé.")
        
        print(f"Création du DataFrame complet {corpus_df_file_path}...")
        corpus_df = pd.DataFrame.from_dict(merged_dict, orient='index')
        corpus_df.to_csv(corpus_df_file_path)
        print(f"\nDataFrame sauvegardé dans {corpus_df_file_path}.\n")
