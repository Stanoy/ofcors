'''Oral French COreference Resolution System'''
# On récupère le dernier tag de git correspondant au projet, on on calcule la version du projet dans un format pre-release
# et conforme aux recommandations PEP 440.

import dunamai as _dunamai
from dunamai import Version, Style
import os
# import ofcors
"""
__version__ = "0.3.4"
"""

__version__ = _dunamai.get_version("ofcors", third_choice=_dunamai.Version.from_any_vcs).serialize(metadata=False, style=_dunamai.Style.Pep440)
#__version__ = Version.from_git().serialize(metadata=False, style=Style.Pep440)
if "post" in __version__ or "dev0" in __version__:
    __version__ = _dunamai.get_version("ofcors", third_choice=_dunamai.Version.from_any_vcs).serialize(metadata=False, dirty=True, style=_dunamai.Style.Pep440, bump=True)
    #__version__ = Version.from_git().serialize(metadata=False, dirty=True, style=Style.Pep440, bump=True)
