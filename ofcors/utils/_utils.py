import os

def get_path_to_ofcors(path):
    path_to_parent_dir, cur_f_name = os.path.split(path)
    _, parent_dir_name = os.path.split(path_to_parent_dir)
    if cur_f_name == "ofcors" and parent_dir_name == "ofcors":
        return path_to_parent_dir;
    else:
        return get_path_to_ofcors(path_to_parent_dir);
    
def get_path_of_file_in(file_name, top="~"):
    for root, dirs, files in os.walk(os.path.expanduser(top)):
        if file_name in files:
            return str(os.path.join(root, file_name))
    raise ValueError(f"No file named {file_name} found in the fs tree, from {top}.")
