'''Oral French COreference Resolution System'''

from ._chainers import *
from ._pairsgenerators import *

__all__ = ['closest_first_coreference_chains', 'best_first_coreference_chains', 'graph_coreference_chains',
           'generate_consecutive_pairs', 'generate_permutations_pairs', 'generate_combinations_pairs', 'generate_window_pairs']
