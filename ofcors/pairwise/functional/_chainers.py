""" Module chainers.

Décrit un ensemble de méthode de chainage de mentions.
"""

import json
import networkx as nx
import pandas as pd
import time
import os


def closest_first_coreference_chains(dataframe, dist_column='TRUE_DISTANCE_MENTION'):
    # Création du graphe pour la construction de la chaîne
    G = nx.Graph()
    
    pred_as_pairs_dataframe = dataframe[dataframe["PREDICTION"] == 1]
    
    # -- Création du graphe des chaînes de coréférence
    # Parcours des paires classées comme coréférentes.
    pos_pairs_with_ids_and_dist = pred_as_pairs_dataframe[['LEFT_NAME', 'RIGHT_NAME', dist_column]]
    for right_id in pos_pairs_with_ids_and_dist['RIGHT_NAME'].unique().tolist():
        right_id_antecedents = pos_pairs_with_ids_and_dist[(pos_pairs_with_ids_and_dist['RIGHT_NAME'] == right_id)]
        right_id_antecedents = right_id_antecedents.drop_duplicates()
        if right_id_antecedents.index.size == 1:
            # Si on n'a qu'un seul antécédent, c'est gagné
            # Même si la seule paire qu'on a est une paire de mentions identiques,
            # on n'aura pas d'autres candidats, et ajouter une arête
            # entre une mention et elle-même n'altèrera pas sa chaine.
            # Ça ne fera qu'ajouter le noeud au graphe.
            closest_antecedent_row = right_id_antecedents
        else:
            # Il n'y a que si on a plus d'un antécédent candidat qu'on peut se permettre de retirer
            # les paires de deux mentions identiques
            # On ne peut pas avoir plusieurs fois la même paire (au sens des ids et de la distance)
            # car on a retiré les doublons. Donc plus d'un antécédent signifie qu'il existe au moins
            # un antécédent non négatif.
            
            # Suppression des doublons
            right_id_antecedents = right_id_antecedents[right_id_antecedents['LEFT_NAME'] != right_id_antecedents['RIGHT_NAME']]
            
            closest_antecedent_row = right_id_antecedents.iloc[[right_id_antecedents[dist_column].argmin()]]
        ant_idx, ant_row = next(closest_antecedent_row.iterrows())
        # Ajout d'une arête dans le graphe.
        G.add_edge(ant_row['LEFT_NAME'], right_id)
    
    # -- Assemblage des chaines
    # Une composante connexe = une chaîne
    coreference_chains = {chain_id:list(cc_nodes) for chain_id, cc_nodes in enumerate(list(nx.connected_components(G)))}
    
    # -- Ajout de tous les singletons
    # Si la mention n'a pas de lien d'elle-même même vers un antécédent noté comme coréférent, alors c'est un singleton
    if len(list(coreference_chains.keys())) > 0:
        chain_id = list(coreference_chains.keys())[-1] + 1
    else:
        chain_id = 0
    for mention_name in dataframe['RIGHT_NAME'].unique().tolist():
        if mention_name not in list(G.nodes):
            coreference_chains[chain_id] = [mention_name]
            chain_id += 1
            
    return(coreference_chains);


def best_first_coreference_chains(dataframe, prob_column='PROBABILITY'):
    # Création du graphe pour la construction de la chaîne
    G = nx.Graph()
    
    pred_as_pairs_dataframe = dataframe[dataframe["PREDICTION"] == 1]
    
    # -- Création du graphe des chaînes de coréférence
    # Parcours des paires classées comme coréférentes.
    pos_pairs_with_ids_and_dist = pred_as_pairs_dataframe[['LEFT_NAME', 'RIGHT_NAME', prob_column]]
    for right_id in pos_pairs_with_ids_and_dist['RIGHT_NAME'].unique().tolist():
        right_id_antecedents = pos_pairs_with_ids_and_dist[(pos_pairs_with_ids_and_dist['RIGHT_NAME'] == right_id)]
        best_antecedent_row = right_id_antecedents.iloc[[right_id_antecedents[prob_column].argmax()]]

        idx_best, row_best = next(best_antecedent_row.iterrows())
        # Ajout d'une arête dans le graphe.
        G.add_edge(row_best['LEFT_NAME'], right_id)
        
    # -- Assemblage des chaines
    # Une composante connexe = une chaîne
    coreference_chains = {chain_id:list(cc_nodes) for chain_id, cc_nodes in enumerate(list(nx.connected_components(G)))}
    
    # -- Ajout de tous les singletons
    # Si la mention n'a pas de lien d'elle-même même vers un antécédent noté comme coréférent, alors c'est un singleton
    if len(list(coreference_chains.keys())) > 0:
        chain_id = list(coreference_chains.keys())[-1] + 1
    else:
        chain_id = 0
    all_left = set(dataframe['LEFT_NAME'].unique().tolist())
    all_right = set(dataframe['RIGHT_NAME'].unique().tolist())
    left_firsts = set([mention for mention in all_left if mention not in all_right])
    for mention_name in set(all_right.union(left_firsts)):
        if mention_name not in list(G.nodes):
            coreference_chains[chain_id] = [mention_name]
            chain_id += 1
            
    return(coreference_chains);
    

def graph_coreference_chains(dataframe):
    pred_as_pairs_dataframe = dataframe.loc[dataframe["PREDICTION"] == 1]

    # Création du graphe pour la construction de la chaîne
    G = nx.Graph()
    
    pos_pairs_with_ids_and_dist = pred_as_pairs_dataframe[['LEFT_NAME', 'RIGHT_NAME']]
    # Construction du graphe des relations entre les paires notées comme coréférentes
    for idx_row, row in pos_pairs_with_ids_and_dist.iterrows():
        left_name = pred_as_pairs_dataframe.at[idx_row, "LEFT_NAME"]
        right_name = pred_as_pairs_dataframe.at[idx_row, "RIGHT_NAME"]
        G.add_edge(str(left_name), str(right_name))
    
    coreference_chains = {str(chain_id):list(cc_nodes) for chain_id, cc_nodes in enumerate(list(nx.connected_components(G)))}
    
    # -- Ajout de tous les singletons
    # Si la mention n'a pas de lien d'elle-même même vers un antécédent noté comme coréférent, alors c'est un singleton
    if len(list(coreference_chains.keys())) > 0:
        chain_id = list(coreference_chains.keys())[-1] + 1
    else:
        chain_id = 0
    for mention_name in dataframe['RIGHT_NAME'].unique().tolist():
        if mention_name not in list(G.nodes):
            coreference_chains[chain_id] = [mention_name]
            chain_id += 1
    
    return coreference_chains;
