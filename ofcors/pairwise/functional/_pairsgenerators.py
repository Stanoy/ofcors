import itertools

def generate_consecutive_pairs(sorted_mentions_names):
    return {(sorted_mentions_names[mention_idx], sorted_mentions_names[mention_idx+1]) for mention_idx in range(len(sorted_mentions_names) -1)};
    
def generate_permutations_pairs(sorted_mentions_names):
    return set(itertools.permutations(list(sorted_mentions_names), 2));
    
def generate_combinations_pairs(sorted_mentions_names):
    return set(itertools.combinations(reversed(list(sorted_mentions_names)), 2));

def generate_window_pairs(sorted_mentions_names, window_size):
    # -- Parcours de tous les noms de mentions triés
    mentions_pairs = set()
    for index_cur_m, cur_mention_name in enumerate(sorted_mentions_names):
        # Calcul de l'index butoire de la fin de la fenetre
        window_bound = index_cur_m-window_size
        if (window_bound) < 0:
            window_bound = 0
        # Construction de toutes les paires possibles entre la mention courante
        # et les window_size mentions avant elle dans l'ordre décroissant
        for index_left in range(index_cur_m, window_bound, -1):
            mentions_pairs.add((sorted_mentions_names[index_left-1], cur_mention_name))
    
    return mentions_pairs;
