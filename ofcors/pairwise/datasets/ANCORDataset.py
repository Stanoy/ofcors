import pandas as pd
from copy import deepcopy

class PairsDataset(pd.DataFrame):

    def __init__(self, truth_column_name, **dfkwargs):
        super().__init__(**dfkwargs)
        self.truth_column_name = truth_column_name
    
    
    
    def get_datas(self):
        return self.drop(self.truth_column_name, axis=1, inplace=True);
        
    
    
    def get_labels(self, with_index=False):
        if with_index:
            return self[self.truth_column_name]
        else:
            return self[self.truth_column_name].tolist()
    
    
    
    def split(self):
        return (self.get_datas(), self.get_labels());
    
    
    
    def get_pos_pairs(self):
        return self[self[self.truth_column_name] == 1];
    
    
    
    def get_neg_pairs(self):
        return self[self[self.truth_column_name] == 0];
    
          
    
    def replace_NA(self, value="VOID", inplace=False):
        return self.fillna(value, inplace=inplace);
            
    
    
    def to_numeric(self, inplace=False):
        """ Méthode convert_columns_to_numeric.
        
        Converti les champs textuels en valeurs numériques uniques.
        """
        textual_values = ['ADJ', 'ADP', 'PUNCT', 'ADV', 'AUX', 'SYM', 'INTJ', 'CCONJ', 'X', 'NOUN', 'DET', 'PROPN', 'NUM', 'VERB', 'PART', 'PRON', 'SCONJ', 'SPACE', #POS tag spacy
                            'LOC', 'MISC', 'ORG', 'PER', #EN tag spacy
                            'UNK', #EN inconnue
                            'M', 'F', 'SG', 'PL', #G&N ancor annot
                            'Fem', 'Masc', 'Sing', 'Plur', #G&N spacy
                            'PR', 'N', #type ancor annot
                            'EXPL', 'INDEF', 'DEF_DEM', 'DEF_SPLE', 'EVENT', #DEF ancor annot
                            'FONC', 'LOC', 'PERS', 'ORG', 'PROD', 'TIME', 'AMOUNT', 'EVENT', #EN ancor annot
                            'NO', 'YES', #GP spacy et ancor annot
                            'VOID' # for NaN values
                            ]
        
        return self.replace(textual_values, range(1, len(textual_values)+1), inplace=inplace);
    
    
    
    def save(self, path):
        self.to_excel(path);
