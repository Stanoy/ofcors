import click
import os
import json
from .modules import MentionDetector
from .modules import PairsGenerator
from .modules import DataframeBuilder
from .modules import PairsClassifier
from .modules import MultiPairsClassifier
from .modules import Chainer
from .modules import ChainsInterpreter




@click.command()
@click.argument('text_or_path')
@click.option('--text', '-t', 'entry_type',
              type=str, flag_value='text', default=True)
@click.option('--file', '-f', 'entry_type', flag_value='file')
@click.option('--mention-detector', '-d', 'mention_detector_path',
              type=click.Path(exists=True))
@click.option('--tokenizer', '-k', 'tokenizer', default="spacy",
              type=str, show_default=True)
@click.option('--output-dir', '-o', 'out_dir', default=os.path.join('.', 'ofcors_outputs'), 
              type=click.Path(), show_default=True)
@click.option('--pairs-mode', '-p', 'pairs_mode', default='window',
              type=str, show_default=True)
@click.option('--window-size', '-w', 'window_size', default=30,
              type=int, show_default=True)
@click.option('--classifier', '-c', 'classifier', default='multi',
              type=str, show_default=True)
@click.option('--chaining-mode', '-m', 'chaining_mode', default='best_first',
              type=str, show_default=True)
def main_entry_point(text_or_path, entry_type, mention_detector_path, tokenizer,
                     out_dir, pairs_mode, window_size, classifier, chaining_mode):
    
    tokens_file_path = os.path.join(os.path.join(os.path.join(".", "ofcors_outputs"), "mentions_detection"), "tokens.json")
    
    d = MentionDetector(mention_detector_path, out_dir, tokenizer)
    p = PairsGenerator(pairs_mode, window_size)
    dfb = DataframeBuilder(tokens_file_path, out_dir)
    if classifier=="multi":
        c = MultiPairsClassifier(out_dir)
    else:
        c = PairsClassifier(out_dir, classifier)
    chainer = Chainer(chaining_mode)
    
    chains = chainer(c(dfb(*p(d(text_or_path)))))
    scorch_format_chains = {"type":"clusters", "clusters":chains}
    
    res_file_path = os.path.join(out_dir, 'resulting_chains.json')
    click.echo(f"Chains writing in {res_file_path}...")
    with open(res_file_path, 'w', encoding='utf-8') as res_file:
        json.dump(scorch_format_chains, res_file)
    
    click.echo("Chaines textuelles :")
    ci = ChainsInterpreter()
    ci(chains)
    
    click.echo("Au format Scorch :")
    click.echo(scorch_format_chains)

if __name__ == "__main__":

    main_entry_point()
