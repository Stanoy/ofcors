from ._pairsgenerator import PairsGenerator
# For preconditions
from preconditions import preconditions
# Spacy model for mentions features computing
import spacy
# For data formatting
import pandas as pd
import numpy as np
import json
# For string computing
import re
import string
# For fs gestion
import os

class DataframeBuilder():
    """ Classe DataframeBuilder.
    
    Décrit un module de fabrication de dataframes des paires de mentions 
    extraites dans le texte.
    
    Attributes
    ----------
    tokens_file_path : str
        Le chemin vers le fichier contenant le dictionnaire des tokens du texte brut.
    df_name : str
        Le nom du dataframe à produire.
    dataframe_output_folder : str
        Le chemin du répertoire dans lequel chercher ou placer le repertoire
        `dataframes`, pour stocker le fichier contenant le dataframe produit.
    post_convert : bool
        Détermine si une post-conversion est effectuée sur les traits des paires.
    spacy_model : spacy.lang.fr.French
        Le modèle de langue spaCy utilisé pour calculer les traits de chaque mention.
    """
    
    @preconditions(lambda tokens_file_path: (isinstance(tokens_file_path, str) or
                                              (tokens_file_path is None)),
                   lambda dataframe_output_folder: (isinstance(dataframe_output_folder, str) or 
                                                    (dataframe_output_folder is None)),
                   lambda df_name: isinstance(df_name, str) or (df_name is None),
                   lambda post_convert: isinstance(post_convert, bool))
    def __init__(self, tokens_file_path=None, dataframe_output_folder=None, df_name=None, post_convert=True):
        """ Méthode __init__
        
        Constructeur de DataframeBuilder.

        Parameters
        ----------
        tokens_file_path : str, optional
            Le chemin vers le fichier contenant le texte brut.
            Par défaut, va chercher le fichier `./ofcors_outputs/mentions_detection/tokens.json`.
        df_name : str, optional
            Le nom du fichier contenant le dataframe à produire.
            Vaut `pairs_df.csv` par défaut.
        dataframe_output_folder : str, optional
            Le chemin du répertoire dans lequel chercher ou placer le repertoire
            `dataframes`, pour stocker le fichier contenant le dataframe produit.
            Par défaut, vaut `./ofcors_outputs/`.
        post_convert : bool, optional
            Détermine si une post-conversion numérique est effectuée sur les traits des paires.
            Vaut `True` par défaut.
        """
        if tokens_file_path is not None:
            self.tokens_file_path = tokens_file_path
        else:
            self.tokens_file_path = os.path.join(os.path.join(os.path.join(".", "ofcors_outputs"), "mentions_detection"), "tokens.json")
           
        if df_name is not None:
            self.df_name = df_name
        else:
            self.df_name = "pairs_df.csv"
            
        if dataframe_output_folder is not None:
            self.dataframe_output_folder = os.path.join(dataframe_output_folder, "dataframes")
        else:
            self.dataframe_output_folder = os.path.join(os.path.join(".", "ofcors_outputs"), "dataframes")
        
        self.post_convert = post_convert
        
        self.spacy_model = spacy.load("fr_core_news_lg")
        
        
    
    def __call__(self, sorted_mentions_dict, pairs_set=None):
        """ Méthode __call__.
        
        Rend callable les instances de DataframeBuilder.
        
        Appelle simplement la méthode `self.build` avec les mêmes arguments
        et retourne son résultat.
        Construit un dataframe de paires de mentions à partir du dictionnaire
        ordonné des mentions du texte `sorted_mentions_dict` et de l'ensemble 
        des paires d'identifiants de mention `pairs_set`.
        Appelle simplement la méthode `self.build` avec les mêmes arguments
        et en retourne son résultat.
        
        Parameters
        ----------
        sorted_mentions_dict : dict
            Le dictionnaire ordonné des mentions du texte.
        pairs_set : set
            L'ensemble des paires d'identifiants de mention.
            
        Returns
        -------
        pandas.DataFrame
            Le dataframe de paires de mentions avec leur ensemble de traits.
        """
        return self.build(sorted_mentions_dict, pairs_set);
       
    
    
    def build(self, sorted_mentions_dict, pairs_set=None):
        """ Méthode build.
        
        Construit un dataframe de paires de mentions à partir du dictionnaire
        ordonné des mentions du texte `sorted_mentions_dict` et de l'ensemble 
        des paires d'identifiants de mention `pairs_set`.
        
        Parameters
        ----------
        sorted_mentions_dict : dict
            Le dictionnaire ordonné des mentions du texte .
        pairs_set : set
            L'ensemble des paires d'identifiants de mention.
            
        Returns
        -------
        pandas.DataFrame
            Le dataframe de paires de mentions avec leur ensemble de traits.
        """
        if pairs_set is None:
            # -- Création des paires
            pairs_generator = PairsGenerator()
            _, _pairs_set = pairs_generator.predict(sorted_mentions_dict)
        else:
            _pairs_set = pairs_set.copy()
        
        # -- Calcul des traits de mention
        # On utilise le modèle spacy associé à l'objet pour le calcul des traits
        # de mention.
        recomputed_sorted_mentions_dict = self.__recompute(sorted_mentions_dict)
        
        # -- Filtrage des paires qui ne peuvent être coréférentes. (mentions embedded)
        # Deux mentions incluses l'une dans l'autre ne peuvent être coréférentes.
        # On ne présente pas ces paires au classifieur.
        _pairs_set = self.__filter_embedded_pairs(_pairs_set, recomputed_sorted_mentions_dict)
        
        if len(_pairs_set) > 0:
            # -- Génération du Dataframe
            df_as_dict = self.__generate_dataframe(_pairs_set, recomputed_sorted_mentions_dict)
            res_df = pd.DataFrame.from_dict(df_as_dict, orient='index')
            
            # -- Post-conversion des valeurs d'attributs
            if self.post_convert:
                res_df = self.__post_convert(res_df)
        else:
            columns_list = ['LEFT_NAME', 'RIGHT_NAME', 'Chain_Id', 'M1_CONTENT', 'M2_CONTENT', 'TRUE_DISTANCE_MENTION',
                'M1_TYPE', 'M1_GP', 'M1_GENRE', 'M1_NB', 'M1_EN', 'M1_NEW', 'M2_TYPE', 'M2_GP', 'M2_GENRE', 'M2_NB', 'M2_EN', 'M2_NEW',
                'ID_FORM', 'ID_SUBFORM', 'ID_EN', 'ID_GP', 'ID_GENRE', 'ID_NB', 'ID_PREV', 'ID_NEXT', 'ID_SPK', 'ID_NEW', 'ID_TYPE', 'EMBEDDED',
                'INCL_RATE', 'COM_RATE', 'DISTANCE_MENTION', 'DISTANCE_WORD', 'DISTANCE_CHAR', 'LAPSED', 'SIMILARITY']
            res_df = pd.DataFrame(columns=columns_list)
        
        # -- Sauvegarde dans un fichier et retour
        if not os.path.exists(self.dataframe_output_folder):
            try:
                os.makedirs(self.dataframe_output_folder)
            except OSError:
                print(f"\nEchec de la creation du repertoire {self.dataframe_output_folder}.")
        
        res_df_file_name = self.df_name if self.df_name.endswith('.csv') else os.path.splitext(self.df_name)[0]+'.csv'
        res_df_file_path = os.path.join(self.dataframe_output_folder, res_df_file_name)
        #print(f"Création du DataFrame complet {res_df_file_path}...")
        res_df.to_csv(res_df_file_path)
        
        return res_df;
    
    
    def __recompute(self, sorted_mentions_dict):
        recomp_sorted_mentions_dict = {}
        # Parcours des mentions
        for num, mention_name in enumerate(list(sorted_mentions_dict.keys()), 1):
            # Copie des traits de la mention courante pour pouvoir les modifier
            mention_feat_dict = dict(sorted_mentions_dict[mention_name])
            
            # -- Modification des traits
            mention_feat_dict["CONTENT"] = ' '.join(list(filter(lambda elt: elt!='<start>' and elt!='<end>',
                                                                mention_feat_dict["CONTENT"])))
            mention_feat_dict["CONTENT"] = " ".join(mention_feat_dict["CONTENT"].strip().split())
            
            # On arrête tout si le contenu de la mention est vide ou erroné
            if (("CONTENT" not in mention_feat_dict) or (len(mention_feat_dict["CONTENT"]) <= 0) or (mention_feat_dict["CONTENT"] is None)):
                continue;
                
            mention_feat_dict["LEFT_CONTEXT"] = ' '.join(list(filter(lambda elt: elt!='<start>' and elt!='<end>',
                                                                     mention_feat_dict["LEFT_CONTEXT"])))
            mention_feat_dict["LEFT_CONTEXT"] = " ".join(mention_feat_dict["LEFT_CONTEXT"].strip().split())
            mention_feat_dict["RIGHT_CONTEXT"] = ' '.join(list(filter(lambda elt: elt!='<start>' and elt!='<end>',
                                                                     mention_feat_dict["RIGHT_CONTEXT"])))
            mention_feat_dict["RIGHT_CONTEXT"] = " ".join(mention_feat_dict["RIGHT_CONTEXT"].strip().split())
            
            mention_feat_dict['START_ID'] = mention_feat_dict.pop('START')
            mention_feat_dict['END_ID'] = mention_feat_dict.pop('END')
            
            mention_feat_dict['NUM'] = num
            
            
            # -- Parsing de la mention
            m1_spacy = self.spacy_model(mention_feat_dict["CONTENT"])
            
            # -- Entitée nommée
            mention_feat_dict['EN'] = "NO"
            if m1_spacy.ents:
                e = next(iter(m1_spacy.ents))
                if ( (e.start_char==0) and (e.end_char==len(mention_feat_dict["CONTENT"])) ):
                    mention_feat_dict['EN'] = e.label_
            
            
            # -- Calcul de POS, GENRE, et NB
            # Groupement du contenu de la mention comme un unique token
            with m1_spacy.retokenize() as retokenizer_m1:
                retokenizer_m1.merge(m1_spacy[0:len(m1_spacy)])
            token = next(iter(m1_spacy))
            mention_feat_dict['TYPE'] = token.pos_ if token.pos_ else "UNK"
            
            # En français, seulement Masc/Fem et Sing/Plur
            # On teste si la valeur est déjà annotée comme NULL (artefact
            # lié à certaines disfluences) parce que le modèle ne pourra pas
            # l'analyser.
            m1_morph = token.morph.to_dict()
            
            if (('GENRE' not in mention_feat_dict) or 
                ('GENRE' in mention_feat_dict and (mention_feat_dict['GENRE'] != "NULL"))):
                mention_feat_dict['GENRE'] = m1_morph['Gender'] if 'Gender' in m1_morph else "UNK"
                
            if (('NB' not in mention_feat_dict) or 
                ('NB' in mention_feat_dict and (mention_feat_dict['NB'] != "NULL"))):
                mention_feat_dict['NB'] = m1_morph['Number'] if 'Number' in m1_morph else "UNK"
            
            
            # -- Recherche des groupes prépositionnels dans la phrase de chaque mention
            m1_sent_spacy = self.spacy_model(mention_feat_dict["LEFT_CONTEXT"]+' '+
                                             mention_feat_dict["CONTENT"]+' '+
                                             mention_feat_dict["RIGHT_CONTEXT"])
            mention_feat_dict['GP'] = "NO"
            for token_m1_sent in m1_sent_spacy:
                if (((token_m1_sent.pos_.upper() == "ADP") or (token_m1_sent.dep_.upper() == "CASE")) and
                        (token_m1_sent.head.text in mention_feat_dict["CONTENT"])):
                    mention_feat_dict['GP'] = "YES"
                    break;
                    
            # -- Calcul de l'embedding
            for token in m1_spacy:
                if token.dep_ == "ROOT":
                    head_token = token
                    break;
            mention_feat_dict["HEAD"] = head_token.text
            
            mention_feat_dict["EMBEDDING"] = head_token.vector.tolist()
            
            
            recomp_sorted_mentions_dict[mention_name] = mention_feat_dict
        
        return recomp_sorted_mentions_dict;
    
    
    def __filter_embedded_pairs(self, pairs_set, recomputed_sorted_mentions_dict):
        new_pairs_set = set()
        for pair in pairs_set:
            left_mention = recomputed_sorted_mentions_dict[pair[0]]
            right_mention = recomputed_sorted_mentions_dict[pair[1]]
            
            m1_content = left_mention["CONTENT"]
            m2_content = right_mention["CONTENT"]
            if len(m1_content) != len(m2_content):
                small_mention = min([left_mention, right_mention], key=lambda m: len(m["CONTENT"]))
                long_mention = max([left_mention, right_mention], key=lambda m: len(m["CONTENT"]))
            else:
                small_mention = left_mention
                long_mention = right_mention
            
            small_mention_interval = small_mention["SPAN_ID"].split("-")
            long_mention_interval = long_mention["SPAN_ID"].split("-")
            
            if not ((int(long_mention_interval[0]) <= int(small_mention_interval[0]) <= int(long_mention_interval[1])) and
                    (int(long_mention_interval[0]) <= int(small_mention_interval[1]) <= int(long_mention_interval[1]))):
                new_pairs_set.add(pair)
        
        return new_pairs_set;
    
    
    def __generate_dataframe(self, pairs_set, sorted_mentions_dict, start_id=0, verbose=True):
        
        def compute_shared_tokens(small, long):
        # Fonction de calcul du nombre de tokens en commun
            _small = list(small)
            _long = list(long)
            shared = []
            for token in _small:
                if token in _long:
                    shared.append(token)
                    _long.remove(token)
            
            return shared;
        
        # Dictionnaire contenant tout le dataframe des données du fichier courant
        dataframe_dict = {}
                
        # Convertisseur de True/False en "YES"/"NO"
        tf2yn = {True: "YES", False: "NO"}
        
        # Parcours de toutes les paires à placer dans le dataframe
        pair_id = start_id
        
        try:
            with open(self.tokens_file_path, 'r') as tokens_file:
                tokens = json.load(tokens_file)
        except EnvironmentError:
            print(f'Impossible to open and read the file self.tokens_file_path {self.tokens_file_path}')
        
        #print(f"{len(pairs_set)} paires à générer")
        for pair in pairs_set:
            #print((f"Paire numéro {pair_id} en cours")
        
            #######################################################################################################
            #                                                                                                     #
            #                                                                                                     #
            #                             Récupération des mentions de la paire                                   #
            #                                                                                                     #
            #                                                                                                     #
            #######################################################################################################
            
            LEFT_NAME = pair[0]
            RIGHT_NAME = pair[1]
            
            # On arrête tout si l'identifiant de la mention gauche ou droite n'existe pas
            if ((LEFT_NAME not in sorted_mentions_dict.keys()) or
                    (RIGHT_NAME not in sorted_mentions_dict.keys())):
                continue;
            
            # Inversion des mentions gauche et droite, si left_index > right_index
            if (sorted_mentions_dict[LEFT_NAME]["NUM"] > sorted_mentions_dict[RIGHT_NAME]["NUM"]):
                LEFT_NAME = pair[1]
                RIGHT_NAME = pair[0]
                
            # Récupération des mentions gauche et droite en JSON avec leurs traits
            left_unit = sorted_mentions_dict[LEFT_NAME]
            right_unit = sorted_mentions_dict[RIGHT_NAME]

            # Récupération des index des mots de départ des mentions gauche et droite
            #left_index = TeiId(left_unit["START_ID"])
            #right_index = TeiId(right_unit["START_ID"])
                
            structural_feat_dict = {}
            structural_feat_dict["LEFT_NAME"] = LEFT_NAME  # ID de mention gauche
            structural_feat_dict["RIGHT_NAME"] = RIGHT_NAME  # ID de mention droite

            structural_feat_dict["M1_HEAD"] = left_unit["HEAD"]
            structural_feat_dict["M2_HEAD"] = right_unit["HEAD"]

            #######################################################################################################
            #                                                                                                     #
            #                                                                                                     #
            #                                           TRAITS DE MENTION                                         #
            #                                                                                                     #
            #                                                                                                     #
            #######################################################################################################
            
            m1_feat_dict = {}
            m2_feat_dict = {}
            
            # Calcul du CONTENT
            m1_feat_dict[f'M1_CONTENT'] = (str(left_unit["CONTENT"]) if (("CONTENT" in left_unit) and (left_unit["CONTENT"] is not None))
                                            else "")
            m2_feat_dict[f'M2_CONTENT'] = (str(right_unit["CONTENT"]) if (("CONTENT" in right_unit) and (right_unit["CONTENT"] is not None))
                                            else "")
            m1_feat_dict[f'M1_CONTENT'] = " ".join(m1_feat_dict[f'M1_CONTENT'].strip().split())
            m2_feat_dict[f'M2_CONTENT'] = " ".join(m2_feat_dict[f'M2_CONTENT'].strip().split())
            
            # -- On arrête tout si le contenu des mention est vide ou erroné
            if ( ((len(m1_feat_dict["M1_CONTENT"]) <= 0) or (m1_feat_dict["M1_CONTENT"] is None)) or
                    ((len(m2_feat_dict["M2_CONTENT"]) <= 0) or (m2_feat_dict["M2_CONTENT"] is None)) ):
                continue;
            
            # Calcul des traits de mention
            #print(("    Calcul des traits de mentions...", end='')
            feat_in_mentions_list = ["CONTENT", "TYPE", "GP", "GENRE", "NB", "EN"]
            for feat in feat_in_mentions_list:
                if feat != "CONTENT":
                    m1_feat_dict[f'M1_{feat}'] = left_unit[feat] if feat in left_unit else None
                    m2_feat_dict[f'M2_{feat}'] = right_unit[feat] if feat in right_unit else None
            #print((" OK")
            
            
            # --- Calcul des distances en mentions, mots et en caractères
            # print(("    Calcul des distances...", end='')
            # Rappel : le slicing est inclusif sur l'index de début, et exclusif sur l'index de fin
            # Mehdi regardait donc left_unit["END_ID"] <= w_id < right_unit["START_ID"]
            # string_text = self.data_source[left_unit["END_ID"]:right_unit["START_ID"]]
            # Dans notre cas, on décide d'être exclusif totalement.
            
            words_list_between = [token for token_id, token in tokens.items() if int(left_unit["END_ID"]) <= int(token_id) and int(token_id) < int(right_unit["START_ID"])]
            string_text_between = ' '.join(words_list_between)
            # Calcul du nombre de caractères entre les mentions
            dist_chars_acc = len(string_text_between)
            #print((" OK")



            #######################################################################################################
            #                                                                                                     #
            #                                                                                                     #
            #                                           TRAITS RELATIONNELS                                       #
            #                                                                                                     #
            #                                                                                                     #
            #######################################################################################################
            
            relationnal_feat_dict = {}
            
            m1 = m1_feat_dict[f'M1_CONTENT']
            m2 = m2_feat_dict[f'M2_CONTENT']
            small = min([m1, m2], key=lambda m: len(m))
            long = max([m1, m2], key=lambda m: len(m))
            # TODO: Remove this part
            if (len(small)<=0) or (len(long)<=0):
            	print("\nsmall =", small)
            	print("long =", long)
            	print("m1 =", m1)
            	print("m2 =", m2, "\n")
            small_tokenized = re.sub("\'", " ", small).strip().split()
            if len(small_tokenized) <= 0:
                small_tokenized = small.strip().split()
            long_tokenized = re.sub("\'", " ", long).strip().split()
            if len(long_tokenized) <= 0:
                long_tokenized = long.strip().split()
            
            
            
            # -- Traits d'inclusion
            
            # Forme complete identique en caractères
            relationnal_feat_dict["ID_FORM"] = tf2yn[m1 == m2]
            
            # Si small est une sous-forme de long (i.e. small incluse strictement dans long)
            # e.g. "Bonjour" est incluse dans "Bonjour à tous les amis"
            #      "Bonjour les amis" n'est PAS incluse dans "Bonjour à tous les amis"
            relationnal_feat_dict["ID_SUBFORM"] = tf2yn[small in long] 
            
            # Taux d'inclusion des tokens
            # (on n'utilise pas set.intersection pour pouvoir calculer le taux 
            # sur des mentions qui contiennent plusieurs fois le meme token)
            # Exemple : long : 'L homme n est pas un loup pour l homme'
            #           small : 'L homme est un loup loup pour l homme'
            #           On voit bien dans ce cas que small (en terme de nb tokens)
            #           n'est pas entièrement inclue dans long, car long ne contient
            #           qu'un seul token "loup".
            len_small = len(small)
            relationnal_feat_dict["INCL_RATE"] = len(compute_shared_tokens(small_tokenized, long_tokenized))/len(small) if len_small > 0 else 0.
            # Taux de tokens communs
            small_tokenized_set = set(small_tokenized)
            long_tokenized_set = set(long_tokenized)
            len_union = len(small_tokenized_set.union(long_tokenized_set))
            relationnal_feat_dict["COM_RATE"] = len(small_tokenized_set.intersection(long_tokenized_set))/len_union if len_union > 0 else 0.
            
            
            
            # -- Traits d'identité
            
            # Identicité du type d'EN
            relationnal_feat_dict["ID_EN"] = tf2yn[(m1_feat_dict[f'M1_EN'] == m2_feat_dict[f'M2_EN'])] 
            
            # Identicité du GP
            relationnal_feat_dict["ID_GP"] = tf2yn[(m1_feat_dict[f'M1_GP'] == m2_feat_dict[f'M2_GP'])]
            
            # Identicité du genre
            if (m1_feat_dict[f'M1_GENRE'] == "UNK") or (m2_feat_dict[f'M2_GENRE'] == "UNK"):
                # Si un des deux ou les deux sont UNK, indetermine
                relationnal_feat_dict["ID_GENRE"] = "UNK"
            else:
                # Cas MASC/FEM/NULL
                relationnal_feat_dict["ID_GENRE"] = tf2yn[(m1_feat_dict[f'M1_GENRE'] == m2_feat_dict[f'M2_GENRE'])]
            
            # Identicité du nombre
            if (m1_feat_dict[f'M1_NB'] == "UNK") or (m2_feat_dict[f'M2_NB'] == "UNK"):
                # Si un des deux ou les deux sont UNK, indetermine
                relationnal_feat_dict["ID_NB"] = "UNK"
            else:
                # Cas SING/PLUR
                relationnal_feat_dict["ID_NB"] = tf2yn[(m1_feat_dict[f'M1_NB'] == m2_feat_dict[f'M2_NB'])]
            
            # Identicité du previous
            m1_prev = left_unit["PREVIOUS"] if "PREVIOUS" in left_unit else "UNK"
            m2_prev = right_unit["PREVIOUS"] if "PREVIOUS" in right_unit else "UNK"
            if (m1_prev == "UNK") or (m2_prev == "UNK"):
                # Si un des deux ou les deux sont UNK, indetermine
                relationnal_feat_dict["ID_PREV"] = "UNK"
            else:
                # Cas général
                relationnal_feat_dict["ID_PREV"] = tf2yn[(m1_prev == m2_prev)] 
            
            # Identicité du next
            m1_next = left_unit["NEXT"] if "NEXT" in left_unit else "UNK"
            m2_next = right_unit["NEXT"] if "NEXT" in right_unit else "UNK"
            if (m1_next == "UNK") or (m2_next == "UNK"):
                # Si un des deux ou les deux sont UNK, indetermine
                relationnal_feat_dict["ID_NEXT"] = "UNK"
            else:
                # Cas général
                relationnal_feat_dict["ID_NEXT"] = tf2yn[(m1_next == m2_next)]
                                                     
            # Identicité du speaker
            m1_spk = left_unit["SPEAKER"] if "SPEAKER" in left_unit else "UNK"
            m2_spk = right_unit["SPEAKER"] if "SPEAKER" in right_unit else "UNK"
            if (m1_spk == "UNK") or (m2_spk == "UNK"):
                # Si un des deux ou les deux sont UNK, indetermine
                relationnal_feat_dict["ID_SPK"] = "UNK"
            else:
                # Cas général
                relationnal_feat_dict["ID_SPK"] = tf2yn[(m1_spk == m2_spk)]
            
            
            # Enchâssement de m2 dans m1
            # Riche car si vrai, m2 apporte des informations complémentaires
            # à m1, mais SIGNIFIE que les deux ne pourront jamais référer à une même entité.
            left_unit = sorted_mentions_dict[LEFT_NAME]
            right_unit = sorted_mentions_dict[RIGHT_NAME]
            m1_content = m1_feat_dict[f'M1_CONTENT']
            m2_content = m2_feat_dict[f'M2_CONTENT']
            small_mention = min([(m1_content, left_unit), (m2_content, right_unit)], key=lambda elt: len(elt[0]))[1]
            long_mention = max([(m1_content, left_unit), (m2_content, right_unit)], key=lambda elt: len(elt[0]))[1]
            
            small_mention_interval = small_mention["SPAN_ID"].split("-")
            long_mention_interval = long_mention["SPAN_ID"].split("-")
            
            # si small occupe [a, b] et long [A, B], alors small est enchâssée dans long ssi
            # A <= a <= B ET A <= b <= B
            embedded = ((int(long_mention_interval[0]) <= int(small_mention_interval[0]) <= int(long_mention_interval[1])) and
                        (int(long_mention_interval[0]) <= int(small_mention_interval[1]) <= int(long_mention_interval[1])))
            relationnal_feat_dict["EMBEDDED"] = tf2yn[embedded]
            
            
            # -- Traits de distance
            
            # Nombre de mentions entre les deux mentions
            relationnal_feat_dict["DISTANCE_MENTION"] = (int(right_unit["NUM"]) - int(left_unit["NUM"]))-1 # -1 car on veut le nombre de mentions ENTRE les deux
            # Distance en tours de paroles
            #relationnal_feat_dict["DISTANCE_TURN"] = TeiId(right_unit["START_ID"]).u - TeiId(left_unit["START_ID"]).u
            # Nombre de mots entre les mentions
            relationnal_feat_dict["DISTANCE_WORD"] = len(words_list_between) # Nombre de mots ENTRE les mentions
            # Nombre de caractères (espaces comprises) ENTRE les mentions
            relationnal_feat_dict["DISTANCE_CHAR"] = len(string_text_between) 
            
            #print((" OK")
            
            
            
            # -- Similarité cosinus
            m1_vec_head = left_unit["EMBEDDING"]
            m2_vec_head = right_unit["EMBEDDING"]

            dot_product = np.dot(m1_vec_head, m2_vec_head)
            norm_m1_vec_head = np.linalg.norm(m1_vec_head)
            norm_m2_vec_head = np.linalg.norm(m2_vec_head)
            
            if ((float(norm_m1_vec_head)*float(norm_m2_vec_head)) != 0.):
                similarity = (float(dot_product) / (float(norm_m1_vec_head)*float(norm_m2_vec_head)))
                relationnal_feat_dict['LAPSED'] = 0
            else :
                similarity = 0.
                relationnal_feat_dict['LAPSED'] = 1
                
            relationnal_feat_dict["SIMILARITY"] = similarity
            
            
            #######################################################################################################
            #                                                                                                     #
            #                                                                                                     #
            #                                Ajout de la paire avec ses attributs                                 #
            #                                                                                                     #
            #                                                                                                     #
            #######################################################################################################
            
            full_row_dict = {**structural_feat_dict, **m1_feat_dict, **m2_feat_dict, **relationnal_feat_dict}
            dataframe_dict[pair_id] = full_row_dict
            #print(" OK")
            #if verbose:
            #   print(f"\rPaire numéro {pair_id} finie", end="")
            pair_id += 1
        
        if verbose:
            print()
            
        return dataframe_dict;
    
    
    def __post_convert(self, df, unused_feats=['DISTANCE_TURN'], with_infos=True):
        def log_scaling(dist_col, base):
            return np.floor(np.log(dist_col, where=(dist_col!=0.)) / np.log(base))
        
        def linear_scaling(dist_col, bracket_size):
            return np.round(dist_col/np.float(bracket_size))
        
        def type_tagset_minimiser(elt):
            if (elt != 'NOUN') and (elt != 'PRON'):
                return 'UNK'
            else:
                return elt
                
        
        tf2yn = {True:"YES", False:"NO"}
        def id_type(m1_type, m2_type):
            if (m1_type == 'UNK') or (m2_type == 'UNK'):
                # Si un des deux ou les deux sont UNK, indetermine
                return 'UNK'
            else:
                # Cas général
                return tf2yn[(m1_type == m2_type)]
          
        
        # Liste des valeurs textuelles possibles trouvables dans le dataframe
        # On a mapé en dur la position (donc le nombre qui va être associé) de chaque tag de spaCy,
        # de manière à ce que ça corresponde aux valeurs numériques de l'annotation de ANCOR.
        # TODO : Faire en sorte que ce ne soit plus en dur
        values_list = [['NOUN', 'PRON', 'UNK', 'NULL'], #POS tag spacy
                       ['YES', 'NO', 'UNK'],
                       ['Masc', 'Fem', 'UNK'],
                       ['Sing', 'Plur', 'UNK'],
                       ['LOC', 'ORG', 'PER', 'MISC', '', '', '', '', 'NO'],#EN tag spacy TODO: MISC value à mieux gérer
                       #['YES', 'NO', 'UNK'],#NEW
                       ['NOUN', 'PRON', 'UNK', 'NULL'], #POS tag spacy
                       ['YES', 'NO', 'UNK'],
                       ['Masc', 'Fem', 'UNK'],
                       ['Sing', 'Plur', 'UNK'],
                       ['LOC', 'ORG', 'PER', 'MISC', '', '', '', '', 'NO'],#EN tag spacy TODO: MISC value à mieux gérer
                       #['YES', 'NO', 'UNK'],#NEW
                       ['YES', 'NO', 'UNK'], # Pour les valeurs booléennes
                       ['YES', 'NO', 'UNK'],
                       ['YES', 'NO', 'UNK'],
                       ['YES', 'NO', 'UNK'],
                       ['YES', 'NO', 'UNK'],
                       ['YES', 'NO', 'UNK'],
                       ['YES', 'NO', 'UNK'],
                       ['YES', 'NO', 'UNK'],
                       ['YES', 'NO', 'UNK'],
                       ['YES', 'NO', 'UNK'],
                       ['YES', 'NO', 'UNK'],
                       ['YES', 'NO', 'UNK'],
                       ['YES', 'NO', 'UNK']
                      ]
        
        _df = df.copy()
        
        # Suppression des colonnes "unnamed"
        _df = _df.loc[:, 'LEFT_NAME':] # On retire les colonnes "Unnamed : 0"
        
        # Suppression des colonnes non-utiles
        df_converted = _df.drop([existing_unused_feat for col_name in unused_feats if col_name in _df.columns],
                                        axis=1, inplace=False)
        
        # Remplacement des NaN des colonnes de GP par "UNK"
        df_converted["M1_GP"] = df_converted["M1_GP"].fillna('UNK', inplace=False)
        df_converted["M2_GP"] = df_converted["M2_GP"].fillna('UNK', inplace=False)
        
        # Copie des distances réelles dans la colonne 'TRUE_MENTION_DISTANCE' (pour chainage)
        df_converted['TRUE_DISTANCE_MENTION'] = df_converted['DISTANCE_MENTION'].copy()
        
        # Conversion des distances (catégorisation dans des brackets)
        for dist_col in filter(lambda col: col.startswith('DISTANCE_'), df_converted.columns):
            #print(dist_col)
            df_converted[dist_col] = log_scaling(df_converted[dist_col], 2).astype(int)
            #df_converted[dist_col] = linear_scaling(df_converted[dist_col], 10)
        
        # Conversion des taux (catégorisation dans des brackets)
        for rate_col in filter(lambda col: col.endswith('_RATE'), df_converted.columns):
            #print(rate_col)
            df_converted[rate_col] = np.floor(np.round(df_converted[rate_col], 1)*10).astype(int)
            
        
        # Réduction du type tagset
        for type_col in filter(lambda col: col.endswith('_TYPE'), df_converted.columns):
            #print(type_col)
            int_value_map = {int_val:val for int_val, val in enumerate(values_list[0])}
            df_converted[type_col].replace(int_value_map, inplace=True)
            df_converted[type_col] = df_converted[type_col].map(type_tagset_minimiser)
        
        # Ajustement de l'ID_TYPE
        df_converted['ID_TYPE'] = df_converted[['M1_TYPE','M2_TYPE']].apply(lambda x: id_type(x['M1_TYPE'], x['M2_TYPE']), axis=1)
        
        
        # Réordonnancement des colonnes de features
        meta_infos_cols = [col_name for col_name in ['LEFT_NAME', 'RIGHT_NAME', 'Chain_Id', 'M1_HEAD', 'M2_HEAD', 'M1_CONTENT', 'M2_CONTENT', 'TRUE_DISTANCE_MENTION']
                           if col_name in df_converted.columns]
        m1_cols = [col for col in df_converted.columns.tolist() if col.startswith('M1_') and not col.endswith('_CONTENT') and not col.endswith('_HEAD')]
        m2_cols = [col for col in df_converted.columns.tolist() if col.startswith('M2_') and not col.endswith('_CONTENT') and not col.endswith('_HEAD')]
        id_cols = [col for col in df_converted.columns.tolist() if col.startswith('ID_')]
        embedded_cols = ['EMBEDDED']
        rate_cols = [col for col in df_converted.columns.tolist() if col.endswith('_RATE')]
        dist_cols = [col for col in df_converted.columns.tolist() if col.startswith('DISTANCE_')]
        lapsed_col = ['LAPSED']
        sim_col = ['SIMILARITY']
        truth_col = ['IS_CO_REF']
        feats_cols = [col_name for col_name in m1_cols+m2_cols+id_cols+embedded_cols+rate_cols+dist_cols+lapsed_col+sim_col+truth_col
                      if col_name in df_converted.columns]
        df_converted = df_converted[meta_infos_cols+feats_cols]
    
        # Conversion en valeurs numériques
        i=0
        for column in m1_cols+m2_cols+id_cols+embedded_cols:
            value_int_map = {val:int_val for int_val, val in enumerate(values_list[i])}
            df_converted[column] = df_converted[column].replace(value_int_map, inplace=False)
            i+=1
            
        # Conversion des types en int
        df_converted[feats_cols] = df_converted[feats_cols].astype(int)
            
        if with_infos:
            return df_converted.copy();
        else:
            return df_converted.drop(meta_infos_cols, axis=1, inplace=False);
