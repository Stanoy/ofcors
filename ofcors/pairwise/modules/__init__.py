'''Oral French COreference Resolution System'''

from ._mentiondetector import MentionDetector
from ._pairsgenerator import PairsGenerator
from ._dataframebuilder import DataframeBuilder
from ._pairsclassifier import PairsClassifier
from ._multipairsclassifier import MultiPairsClassifier
from ._chainer import Chainer
from ._chainsinterpreter import ChainsInterpreter

__all__ = ['MentionDetector', 'PairsGenerator', 'DataframeBuilder',
           'PairsClassifier', 'MultiPairsClassifier', 'Chainer', 'ChainsInterpreter']
