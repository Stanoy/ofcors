from .. import  functional as F
from preconditions import preconditions

class PairsGenerator():
    """ Classe PairsGenerator.
    
    Décrit un module de génération de paires de mentions.
    
    Attributes
    ----------
    mode : str
        Le mode de construction des paires de mention, dans {'consecutive', 'permutations', 'combinations', 'window'}
    window_size : int or None
        La taille de la fenêtre du modèle, si `mode=='window'`
    """
    @preconditions(lambda mode: mode in ['consecutive', 'permutations', 'combinations', 'window'],
                   lambda window_size: isinstance(window_size, int) or (window_size is None))
    def __init__(self, mode='consecutive', window_size=None):
        """ Méthode __init__.
        
        Constructeur de PairsGenerator.
        
        Parameters
        ----------
        mode : str, optional
            Le mode de construction des paires de mention, dans {'consecutive', 'permutations', 'combinations', 'window'}
            Vaut 'consecutive' par défaut.
        window_size : int or None, optional
            La taille de la fenêtre du modèle, si `mode=='window'`.

        Raises
        ------
        ValueError
            Si `window_size` vaut `None` alors alors que le `mode=='window'`
        """
        if ((window_size is None) and (mode=="window")):
            raise ValueError(f'window_size value must be specified for a PairsGenerator in mode {mode}.')
        else:
            self.mode=mode
            self.window_size=window_size
    
           
    def __call__(self, sorted_mentions_desc):
        """ Méthode __call__.
        
        Rend callable les instances de PairsGenerator.
        
        Appelle simplement la méthode `self.predict` avec les mêmes arguments
        et retourne son résultat.
        Calcule les paires de mentions à partir de `sorted_mentions_desc`.
        Si `sorted_mentions_desc` est un dictionnaire, construit les paires
        en associant les éléments de la liste ordonnée des clés du dictionnaire.
        Si `sorted_mentions_desc` est une liste, construit les paires en associant
        les éléments de la liste directement. Cela permet par exemple d'obtenir
        des paires de mentions directement, si on a une liste de mentions, plutôt
        que d'obtenir des paires de clés de mentions dans le dictionnaire.

        Parameters
        ----------
        sorted_mentions_desc : dict or list
            Structure contenant les élémentsà associer.

        Returns
        -------
        tuple
            Le couple `(sorted_mentions_desc, pairs_set)` de la structure passée
            en entrée, et l'ensemble de paires générées.
            Cela permet de retrouver le lien entre les paires de clés et le dictionnaire
            de mentions, dans le cas où `sorted_mentions_desc` est de type `dict`.
        """
        return self.predict(sorted_mentions_desc);
        
    
    @preconditions(lambda sorted_mentions_desc: isinstance(sorted_mentions_desc, dict) or isinstance(sorted_mentions_desc, list))
    def predict(self, sorted_mentions_desc):
        """ Méthode predict.
        
        Calcule les paires de mentions à partir de `sorted_mentions_desc`.
        Si `sorted_mentions_desc` est un dictionnaire, construit les paires
        en associant les éléments de la liste ordonnée des clés du dictionnaire.
        Si `sorted_mentions_desc` est une liste, construit les paires en associant
        les éléments de la liste directement. Cela permet par exemple d'obtenir
        des paires de mentions directement, si on a une liste de mentions, plutôt
        que d'obtenir des paires de clés de mentions dans le dictionnaire.

        Parameters
        ----------
        sorted_mentions_desc : dict or list
            Structure contenant les éléments à associer.

        Returns
        -------
        tuple
            Le couple `(sorted_mentions_desc, pairs_set)` de la structure passée
            en entrée, et l'ensemble de paires générées.
            Cela permet de retrouver le lien entre les paires de clés et le dictionnaire
            de mentions, dans le cas où `sorted_mentions_desc` est de type `dict`.
        """
        if isinstance(sorted_mentions_desc, dict):
            sorted_mentions_names = list(sorted_mentions_desc.keys())
        elif isinstance(sorted_mentions_desc, list):
            sorted_mentions_names = sorted_mentions_desc
        else:
            raise ValueError(f"Error : sorted_mentions_desc type ({type(sorted_mentions_desc)}) not accepted. Should be a list or a dict.")
        
        # Dans le cas très spécifique où le texte ne contient qu'une mention,
        # on renvoie seulement la paire de cette seule mention vers elle-même.
        # C'est en effet son seul antécédent possible.
        # TODO: Intégrer cela de manière plus propre dans le pipeline.
        if len(sorted_mentions_names) == 1:
            return {(sorted_mentions_names[0], sorted_mentions_names[0])};
        
        if self.mode=='consecutive':
            return (sorted_mentions_desc, F.generate_consecutive_pairs(sorted_mentions_names));
        elif self.mode=='permutations':
            return (sorted_mentions_desc, F.generate_permutations_pairs(sorted_mentions_names));
        elif self.mode=='combinations':
            return (sorted_mentions_desc, F.generate_combinations_pairs(sorted_mentions_names));
        elif self.mode=='window':
            return (sorted_mentions_desc, F.generate_window_pairs(sorted_mentions_names, self.window_size));
        else:
            raise ValueError(f"Error : self.mode value ({self.mode}) not accepted.")
