from preconditions import preconditions
from .. import functional as F

class Chainer():
    ''' Classe Chainer.
    
    Définit un module de chaînage des paires de coréférences.
    
    Attributes
    ----------
    mode : str
        Le mode de chaînage du module. ('closest_first', 'best_first', 'graph')
    column_name : str
        Le nom de la colonne du dataframe sur laquele s'appuyer pour choisir
        le meilleur antécédant m_b d'une mention donnée m parmi toutes les mentions
        m_i candidates dans l'ensemble des  paires (m_i, m).
    '''

    @preconditions(lambda mode: mode in ['closest_first', 'best_first', 'graph'],
                   lambda column_name: isinstance(column_name, str) or column_name is None)
    def __init__(self, mode='closest_first', column_name=None):
        ''' Méthode __init__.
        
        Constructeur de Chainer.

        Parameters
        ----------
        mode : str, optional
            Le mode de chaînage du module. ('closest_first', 'best_first', 'graph')
            Vaut 'closest_first' par défaut.
        column_name : str, optional
            Le nom de la colonne du dataframe sur laquele s'appuyer pour choisir
            le meilleur antécédant m_b d'une mention donnée m parmi toutes les mentions
            m_i candidates dans l'ensemble des  paires (m_i, m).
            Par défaut, vaut `'TRUE_DISTANCE_MENTION'` si `self.mode=='closest_first'` 
            et `'PROBABILITY'` si `self.mode=='best_first'`.
        '''
        self.mode = mode
        if column_name is None:
            if self.mode=='closest_first':
                self.column_name = 'TRUE_DISTANCE_MENTION'
            elif self.mode=='best_first':
                self.column_name = 'PROBABILITY'
            else:
                self.column_name = None
        else:
            self.column_name = column_name
            
    
    
    def __call__(self, out_pred_df):
        """ Méthode __call__.
        
        Rend callable les instances de Chainer.
        
        Appelle simplement la méthode `self.predict` avec les mêmes arguments
        et retourne son résultat.
        Prédit les chaînes de coréférences à partir du dataframe `out_pred_df`.
        
        Parameters
        ----------
        out_pred_df : pandas.DataFrame
            Le dataframe contenant les paires de mentions avec leur prédiction
            dans la colonne `PREDICTION`.
            
        Returns
        -------
        dict
            Les chaines de coréférence extraites depuis `out_pred_df`.
        """
        return self.predict(out_pred_df);
    
    
    
    def predict(self, out_pred_df):
        """ Méthode predict.
        
        Prédit les chaînes de coréférences à partir du dataframe `out_pred_df`.

        Parameters
        ----------
        out_pred_df : pandas.DataFrame
            Le dataframe contenant les paires de mentions avec leur prédiction
            dans la colonne `PREDICTION`.

        Raises
        ------
        ValueError
            Si le mode de chaînage `self.mode` n'est pas accepté.

        Returns
        -------
        dict
            Les chaines de coréférence extraites depuis `out_pred_df`.

        """
        # Si on n'a qu'une seule mention dans le texte, ou bien deux
        # alors il est possible qu'il n'y ait qu'une seule paire proposée
        # au modèle, selon le mode de selection des paires.
        # Dans ce cas :
        #    - si la seule paire est considérée comme coréférente, alors on aura un seul cluster contenant les deux mentions
        #    - si la seule paire n'est pas considérée comme coréférente, alors
        #       - si les contenus sont identiques : un singleton (cas où on n'a qu'une seule mention dans le texte)
        #       - si les contenus ne sont pas identiques : deux singletons
        # TODO: Intégrer ces cas particuliers plus proprement dans le pipeline.
        if out_pred_df.index.size == 1:
            left_mention_name, right_mention_name = (str(out_pred_df.at[0, "LEFT_NAME"]), str(out_pred_df.at[0, "RIGHT_NAME"]))
            if out_pred_df.at[0, "PREDICTION"] == 1:
                return {"0": [left_mention_name, right_mention_name]};
            else:
                if left_mention_name == right_mention_name:
                    return {"0": [left_mention_name]};
                else:
                    return {"0": [left_mention_name], "1": [right_mention_name]};
        
        
        if ('PREDICTION' in out_pred_df.columns.tolist()) and (self.column_name in out_pred_df.columns.tolist()):
            if self.mode == 'closest_first':
                return F.closest_first_coreference_chains(out_pred_df, self.column_name)
            elif self.mode == 'best_first':
                return F.best_first_coreference_chains(out_pred_df, self.column_name)
            elif self.mode == 'graph':
                return F.graph_coreference_chains(out_pred_df)
            else:
                raise ValueError("Error : self.mode value ({self.mode}) not allowed.")
            
        else:
            raise ValueError(f"Error : The input dataframe must contains, at least, a  'PREDICTION' and '{self.column_name}' in its columns.")
