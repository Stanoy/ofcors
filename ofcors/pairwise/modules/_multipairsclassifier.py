import sklearn
import joblib 
import os
import pandas as pd
import numpy as np
from preconditions import preconditions
from ofcors.utils import get_path_of_file_in
from ofcors.pairwise.modules import PairsClassifier

class MultiPairsClassifier():
    """ Classe MultiPairsClassifier.
    
    Décrit un module de multi-classification de paires de mentions.
    
    Un MultiPairsClassifier est composé de différents modèles, chacun spécialisé
    dans la classification des différents types de relations de coréférence.
    
    - Un modèle spécialisé dans la résolution des paires pronominales (dont la mention gauche est un pronom),
    - un modèle spécialisé dans la résolution des paires directes (non pronominales et dont les têtes lexicales des deux mentions sont identiques),
    - un modèle spécialisé dans la résolution des paires indirectes (le reste).
    
    La selection des paires à passer à chacun de ces classifieurs s'effectue par vérification des conditions énoncées ci-dessus (par déduction du type)
    et non en se basant sur le type d'annotation des paires donné au PairsClassifier.
    
    Attributes
    ----------
    dataframe_output_folder : str
        Le chemin du répertoire dans lequel chercher ou placer (s'il n'existe pas)
        le repertoire `dataframes`, pour stocker le dataframe de prediction produit.
    pro_clf : ofcors.pairwise.modules.PairsClassifier
        Le modèle de classification spécialisé dans la résolution des paires pronominales du module de multi-classification.
    dir_clf : ofcors.pairwise.modules.PairsClassifier
        Le modèle de classification spécialisé dans la résolution des paires pronominales du module de multi-classification.
    ind_clf : ofcors.pairwise.modules.PairsClassifier
        Le modèle de classification spécialisé dans la résolution des paires pronominales du module de multi-classification.
    save_files : bool
        Détermine si le dataframe de prédiction doit être sauvegardé dans un
        fichier, ou non.
    """
    @preconditions(lambda dataframe_output_folder: (isinstance(dataframe_output_folder, str) or 
                                                    (dataframe_output_folder is None)),
                   lambda pro_clf: (isinstance(pro_clf, PairsClassifier) or pro_clf is None),
                   lambda dir_clf: (isinstance(dir_clf, PairsClassifier) or dir_clf is None),
                   lambda ind_clf: (isinstance(ind_clf, PairsClassifier) or ind_clf is None),
                   lambda save_files: isinstance(save_files, bool))
    def __init__(self, dataframe_output_folder=None, pro_clf=None, dir_clf=None, ind_clf=None, save_files=True):
        """ Méthode __init__.
        
        Constructeur de MultiPairsClassifier.

        Parameters
        ----------
        dataframe_output_folder : str, optional
            Le chemin du répertoire dans lequel chercher ou placer (s'il n'existe pas)
            le repertoire `dataframes`, pour stocker le dataframe de prediction produit.
            Par défaut, vaut `./ofcors_outputs`.
        pro_clf : ofcors.pairwise.modules.PairsClassifier, optional
            L'objet de type ofcors.pairwise.modules.PairsClassifier correspondant
            au modèle de classification du multi-classifieur spécialisé dans la 
            résolution des paires pronominales.
            Par défaut, charge le modèle dans le fichier `OFCORS_pronominal_classifier.model`
            trouvé récursivement dans le système de fichier à partir du repertoire personnel.
        dir_clf : ofcors.pairwise.modules.PairsClassifier, optional
            L'objet de type ofcors.pairwise.modules.PairsClassifier correspondant
            au modèle de classification du multi-classifieur spécialisé dans la 
            résolution des paires directes.
            Par défaut, charge le modèle dans le fichier `OFCORS_direct_classifier.model`
            trouvé récursivement dans le système de fichier à partir du repertoire personnel.
        ind_clf : ofcors.pairwise.modules.PairsClassifier, optional
            L'objet de type ofcors.pairwise.modules.PairsClassifier correspondant
            au modèle de classification du multi-classifieur spécialisé dans la 
            résolution des paires indirectes.
            Par défaut, charge le modèle dans le fichier `OFCORS_indirect_classifier.model`
            trouvé récursivement dans le système de fichier à partir du repertoire personnel.
        save_files : bool
            Détermine si le dataframe de prédiction doit être sauvegardé dans un
            fichier, ou non.
        """
        if dataframe_output_folder is not None:
            self.dataframe_output_folder = os.path.join(dataframe_output_folder, "dataframes")
        else:
            self.dataframe_output_folder = os.path.join(os.path.join(".", "ofcors_outputs"), "dataframes")
        
        if pro_clf is not None:
            self.pro_clf = pro_clf
        else :
            self.pro_clf = PairsClassifier(dataframe_output_folder=None, clf=get_path_of_file_in(f'OFCORS_pronominal_classifier.model', "~"), pretrained=False, save_files=False)
        
        if dir_clf is not None:
            self.dir_clf = dir_clf
        else :
            self.dir_clf = PairsClassifier(dataframe_output_folder=None, clf=get_path_of_file_in(f'OFCORS_direct_classifier.model', "~"), pretrained=False, save_files=False)
        
        if ind_clf is not None:
            self.ind_clf = ind_clf
        else :
            self.ind_clf = PairsClassifier(dataframe_output_folder=None, clf=get_path_of_file_in(f'OFCORS_indirect_classifier.model', "~"), pretrained=False, save_files=False)
            
        self.save_files = save_files
    
    
    
    def __call__(self, df):
        """ Méthode __call__.
        
        Rend callable les instances de PairsClassifier.
        
        Appelle simplement la méthode `self.predict` avec les mêmes arguments
        et retourne son résultat.
        Effectue la prédiction de chacune des paires de mentions décrites dans
        le dataframe `df`.
        
        Parameters
        ----------
        df : pandas.DataFrame
            Le dataframe décrivant chacune des paires de mentions à classifier
            comme coréférentes ou non.

        Returns
        -------
        pandas.DataFrame
            Le dataframe `df` avec la valeur de prédiction associée à chaque paire
            dans la colonne `'PREDICTION'` et la probabilité associée dans la 
            colonne `'PROBABILITY'` si `self.clf` le permet.
        """
        return self.predict(df);
    
    
    
    def predict(self, df):
        """ Méthode predict.
        
        Effectue la prédiction de chacune des paires de mentions décrites dans
        le dataframe `df`.
        
        Parameters
        ----------
        df : pandas.DataFrame
            Le dataframe décrivant chacune des paires de mentions à classifier
            comme coréférentes ou non.

        Returns
        -------
        pandas.DataFrame
            Le dataframe `df` avec la valeur de prédiction associée à chaque paire
            dans la colonne `'PREDICTION'` et la probabilité associée dans la 
            colonne `'PROBABILITY'` si `self.clf` le permet.
        """
        
        multi_out_df = df.copy()
        if df.index.size > 0:
            # On a le droit de se baser sur le POS de la mention droite, parce que calculable à l'inférence
            treat_as_pron_idx = multi_out_df.index[multi_out_df["M2_TYPE"] == 1]
            # On a le droit de se baser sur la tête, puisqu'elle est calculable avec Spacy à l'inférence
            treat_as_dire_idx = multi_out_df.index[~multi_out_df.index.isin(treat_as_pron_idx) &
                                                   (multi_out_df["M1_HEAD"] == multi_out_df["M2_HEAD"])]
            # Tout le reste, est considéré comme une relation indirecte.
            treat_as_indi_idx = multi_out_df.index[~multi_out_df.index.isin(treat_as_pron_idx) &
                                                   ~multi_out_df.index.isin(treat_as_dire_idx) &
                                                   ((multi_out_df["M1_HEAD"] != multi_out_df["M2_HEAD"]) & (multi_out_df["M2_TYPE"] != "PR"))]
            assert treat_as_pron_idx.append(treat_as_dire_idx).append(treat_as_indi_idx).sort_values().equals(multi_out_df.index.sort_values())
            
            X = df.loc[:, 'M1_TYPE':]
            
            pron_out_df = self.pro_clf.predict(X.loc[treat_as_pron_idx])
            dire_out_df = self.dir_clf.predict(X.loc[treat_as_dire_idx])
            indi_out_df = self.ind_clf.predict(X.loc[treat_as_indi_idx])
            
            # Dans tous les cas, les predictions sorties possèdent bien une colonne "PREDICTION" de décision de classification
            y_pred_dict = {idx:val for table in [zip(treat_as_pron_idx, pron_out_df['PREDICTION']), # pron_pred['PREDICTION'] est une pd.Series
                                                 zip(treat_as_dire_idx, dire_out_df['PREDICTION']),
                                                 zip(treat_as_indi_idx, indi_out_df['PREDICTION'])]
                           for idx, val in table}
            y_pred = pd.Series(y_pred_dict).sort_index()
            multi_out_df['PREDICTION'] = y_pred
            
            # Si les modèles ont tous sorti des probabilités dans la colonne "PROBABILITY" alors on ajoute ces valeurs
            # Pour rappel : (colonne de prob <=> modèles permettant de calculer une prob)
            if all('PROBABILITY' in sub_clf_out_df.columns.tolist() for sub_clf_out_df in [pron_out_df, dire_out_df, indi_out_df]):
                y_prob_dict = {idx:val for table in [zip(treat_as_pron_idx, pron_out_df['PROBABILITY']), # pron_pred['PROBABILITY'] est une pd.Series
                                                     zip(treat_as_dire_idx, dire_out_df['PROBABILITY']),
                                                     zip(treat_as_indi_idx, indi_out_df['PROBABILITY'])]
                               for idx, val in table}
                y_prob = pd.Series(y_prob_dict).sort_index()
                multi_out_df['PROBABILITY'] = y_prob
            
            
            # Sauvegarde du df de sortie, si demandée
            if self.save_files:
                os.makedirs(self.dataframe_output_folder, exist_ok=True)
                multi_out_df.to_csv(os.path.join(self.dataframe_output_folder, 'out_pred_df.csv'))
                
        else:
            multi_out_df['PREDICTION']=0 # Pour que la colonne soit ajoutée au df de sortie, même s'il ne contient aucun exemple (pour cohésion avec la suite du pipeline)
            # On se base sur la cohérence : (colonne de prob <=> modèles permettant de calculer une prob)
            if all((hasattr(sub_clf.clf, 'predict_proba') and callable(getattr(sub_clf.clf, 'predict_proba', None))) for sub_clf in [self.pro_clf, self.dir_clf, self.ind_clf]):
                multi_out_df['PROBABILITY']=0
                
        
        return multi_out_df;
