import sklearn
import joblib 
import os 	
from preconditions import preconditions
from ofcors.utils import get_path_of_file_in

class PairsClassifier():
    """ Classe PairsClassifier.
    
    Décrit un module de classification de paires de mentions avec un simple classifieur.
    
    Attributes
    ----------
    dataframe_output_folder : str
        Le chemin du répertoire dans lequel chercher ou placer (s'il n'existe pas)
        le repertoire `dataframes`, pour stocker le dataframe de prediction produit.
    clf : str or sklearn.* model object/path
        Le modèle de classification du module de classification.
    save_files : bool
        Détermine si le dataframe de prédiction doit être sauvegardé dans un
        fichier, ou non.
    """
    @preconditions(lambda dataframe_output_folder: (isinstance(dataframe_output_folder, str) or 
                                                    (dataframe_output_folder is None)),
                   lambda clf: (isinstance(clf, str) or os.path.isfile(clf) or ("sklearn" in str(type(clf)))),
                   lambda pretrained: isinstance(pretrained, bool),
                   lambda save_files: isinstance(save_files, bool))
    def __init__(self, dataframe_output_folder=None, clf='random_forest', pretrained=True, save_files=True, **clf_kwargs):
        """ Méthode __init__.
        
        Constructeur de PairsClassifier.

        Parameters
        ----------
        dataframe_output_folder : str, optionl
            Le chemin du répertoire dans lequel chercher ou placer (s'il n'existe pas)
            le repertoire `dataframes`, pour stocker le dataframe de prediction produit.
            Par défaut, vaut `./ofcors_outputs`.
        clf : str, optional
            Le chemin du fichier contenant le modèle de classification à associer
            au module. Dans ce cas, `self.clf` est le modèle chargé à partir de
            ce fichier.
            Peut également décrire un type de classifieur à associer au module parmi
            {'random_forest', 'linear_svm', 'decision_tree'}. Dans ce cas, `self.clf`
            est un modèle sklearn
        pretrained : bool
            Determine si le modèle de type saisi dans `clf` doit être chargé 
            pré-entraîné ou non. (Si `clf in ['random_forest', 'linear_svm', 'decision_tree']`).
            Si vrai, recherche le modèle `OFCORS_{clf}_classifier.model` à partir du
            repertoire personnel.
            Sinon, créer une nouvelle instance de modèle sklearn correspondante
            à la valeur de `clf` avec les arguments passés dans `**kwargs`.
        save_files : bool
            Détermine si le dataframe de prédiction doit être sauvegardé dans un
            fichier, ou non.
        
        Keyword Arguments
        -----------------
        **clf_kwargs : dict
            Les arguments à passer au modèle dans le cas
            `clf in ['random_forest', 'linear_svm', 'decision_tree'] and not pretrained`.

        Raises
        ------
        ValueError
            Si le type de classifieur `clf` saisi n'est ni dans {'random_forest', 'linear_svm', 'decision_tree'}
            ni un chemin de fichier existant, ni un modèle de la librairie Scikit-learn.
        """
        if dataframe_output_folder is not None:
            self.dataframe_output_folder = os.path.join(dataframe_output_folder, "dataframes")
        else:
            self.dataframe_output_folder = os.path.join(os.path.join(".", "ofcors_outputs"), "dataframes")
        
        if isinstance(clf, str) and os.path.isfile(clf):
            self.clf = joblib.load(clf)
        elif "sklearn" in str(type(clf)):
            self.clf = clf
        elif clf=='random_forest':
            if pretrained:
                self.clf = joblib.load(get_path_of_file_in(f'OFCORS_{clf}_classifier.model', "~"))
            else:
                self.clf = sklearn.ensemble.RandomForestClassifier(**clf_kwargs)
        elif clf=='linear_svm':
            if pretrained:
                self.clf = joblib.load(get_path_of_file_in(f'OFCORS_{clf}_classifier.model', "~"))
            else:
                self.clf = sklearn.svm.LinearSVC(**clf_kwargs)
        elif clf=='decision_tree':
            if pretrained:
                self.clf = joblib.load(get_path_of_file_in(f'OFCORS_{clf}_classifier.model', "~"))
            else:
                self.clf = sklearn.tree.DecisionTreeClassifier(**clf_kwargs)
        else:
            raise ValueError('The clf argument value ({clf}) should be a type of classifier, a path to the file containing the model, or a sklearn model.')
        
        self.save_files = save_files
    
    
    
    def __call__(self, df):
        """ Méthode __call__.
        
        Rend callable les instances de PairsClassifier.
        
        Appelle simplement la méthode `self.predict` avec les mêmes arguments
        et retourne son résultat.
        Effectue la prédiction de chacune des paires de mentions décrites dans
        le dataframe `df`.
        
        Parameters
        ----------
        df : pandas.DataFrame
            Le dataframe décrivant chacune des paires de mentions à classifier
            comme coréférentes ou non.

        Returns
        -------
        pandas.DataFrame
            Le dataframe `df` avec la valeur de prédiction associée à chaque paire
            dans la colonne `'PREDICTION'` et la probabilité associée dans la 
            colonne `'PROBABILITY'` si `self.clf` le permet.
        """
        return self.predict(df);
    
    
    
    def predict(self, df):
        """ Méthode predict.
        
        Effectue la prédiction de chacune des paires de mentions décrites dans
        le dataframe `df`.
        
        Parameters
        ----------
        df : pandas.DataFrame
            Le dataframe décrivant chacune des paires de mentions à classifier
            comme coréférentes ou non.

        Returns
        -------
        pandas.DataFrame
            Le dataframe `df` avec la valeur de prédiction associée à chaque paire
            dans la colonne `'PREDICTION'` et la probabilité associée dans la 
            colonne `'PROBABILITY'` si `self.clf` le permet.
        """
        
        out_df = df.copy()
        if df.index.size > 0:
            X = df.loc[:, 'M1_TYPE':]
            y_pred = self.clf.predict(X)
            out_df['PREDICTION'] = y_pred
            if hasattr(self.clf, 'predict_proba') and callable(getattr(self.clf, 'predict_proba', None)): # the second part is not resolved if the first part is false
                out_df['PROBABILITY'] = (self.clf.predict_proba(X)[:, list(self.clf.classes_).index(1)]
                                                if 1 in list(self.clf.classes_) 
                                                else self.clf.predict_proba(X)[:, list(self.clf.classes_).index(0)])
            if self.save_files:
                os.makedirs(self.dataframe_output_folder, exist_ok=True)
                out_df.to_csv(os.path.join(self.dataframe_output_folder, 'out_pred_df.csv'))
        
        else:
            out_df['PREDICTION']=0
            # On n'ajoute cette colonne de probabilité uniquement si le modèle associé permet d'en calculer une.
            # On conserve cela pour la cohérence. (colonne de prob <=> modèle permettant de calculer une prob) 
            if hasattr(self.clf, 'predict_proba') and callable(getattr(self.clf, 'predict_proba', None)):
                out_df['PROBABILITY']=0
        
        return out_df;
    
    
    
    def fit(self, df):
        """ Méthode fit.
        
        Entraine le module de classification sur le dataframe d'entraînement `df`.

        Parameters
        ----------
        df : pandas.DataFrame
            Le dataframe d'entraînement.
        """
        X = df.loc[:, 'M1_TYPE':]
        y = df['IS_CO_REF']
        self.clf.fit(X, y);
        
        
    
    def save(self, clf_path):
        """ Méthode save.
        
        Sauvegarde le modèle de classification `self.clf` du module dans le$
        fichier `clf_path`.
        
        Parameters
        ----------
        clf_path : str
            Chemin du fichier dans lequel sauvegarder le modèle.
        """
        joblib.dump(self.clf, clf_path);
    
    
    def load(self, clf_path):
        """ Méthode load.
        
        Charge le modèle décrit par le fichier `clf_path` dans `self.clf`.
        

        Parameters
        ----------
        clf_path : str
            Chemin du fichier décrivant le modèle.

        Raises
        ------
        ValueError
            Si le chemin spécifié ne représente pas un fichier.
        """
        if os.path.isfile(clf_path):
            self.clf = joblib.load(clf_path);
        else:
            raise ValueError(f"The clf_path argument value ({clf_path}) does not represent a file.")
