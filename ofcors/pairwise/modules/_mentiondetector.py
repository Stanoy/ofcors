import os
import json
import orjson
from preconditions import preconditions
from decofre import detmentions
from decofre.formats import raw_text as drt
from ofcors.utils import get_path_of_file_in
from ofcors.utils import raw_text as ort
import itertools
import spacy_stanza
import stanza
import spacy

class MentionDetector():
    """ Classe MentionDetector.
    
    Décrit un module de detection des mentions dans du texte brut.
    Les mentions retournées par le modules peuvent être retournées selon plusieurs
    formats à la convenance de l'utilisateur.
    
    Attributes
    ----------
    mention_detector_model_path : str
        Le chemin vers le fichier contenant le modèle de detection de mentions.
    output_dir_path : str
        Le chemin du répertoire dans lequel chercher ou placer le repertoire
        `mentions_detection`, pour stocker le fichier contenant les mentions extraites
        ainsi que les fichiers intermédiaires.
    tokenizer : str
        Le nom du modèle de tokenisation du texte brut du detecteur, dans {'spacy', 'stanza', 'spacy_stanza'}.
    mentions_format : str
        Le format de sortie des mentions extraites dans {'decofre', 'ofcors', 'content'}.
    key_type : str
        Le type des clés à utiliser dans le dictionnaire de sortie lorsque `mentions_format=='ofcors'`.
    nlp : spacy.lang.fr.French
        Le modèle spacy_stanza Stanza (StanfordNLP) embarqué dans un pipeline spaCy, utilisé pour la tokenisation.
    """
    @preconditions(lambda mention_detector_model_path: (isinstance(mention_detector_model_path, (str, os.PathLike)) or
                                                       mention_detector_model_path is None),
                   lambda output_dir_path: isinstance(output_dir_path, str) or (output_dir_path is None),
                   lambda tokenizer : isinstance(tokenizer, str) and tokenizer in ['spacy', 'stanza', 'spacy_stanza'],
                   lambda mentions_format: mentions_format in ['decofre', 'ofcors', 'content'],
                   lambda key_type: key_type in ['num', 'span_id'])
    def __init__(self, mention_detector_model_path=None, output_dir_path=None,
                 tokenizer='spacy', mentions_format='ofcors', key_type='num'):
        """ Méthode __init__.
        
        Constructeur de MentionDetector.
        
        Parameters
        ----------
        mention_detector_model_path : str, optional
            Le chemin vers le fichier contenant le modèle de detection de mentions.
            Par défaut, cherche le modèle dans l'arborescence du système à partir
            du repertoire personnel `~`.
        output_dir_path : str, optional
            Le chemin du répertoire dans lequel chercher ou placer le repertoire
            `mentions_detection`, pour stocker le fichier contenant les mentions
            extraites ainsi que les fichiers intermédiaires.
            Par défaut, vaut `./ofcors_outputs`.
        tokenizer : str
            Le nom du modèle de tokenisation du texte brut du detecteur, dans {'spacy', 'stanza', 'spacy_stanza'}.
            Vaut 'spacy' par défaut.
        mentions_format : str, optional
            Le format de sortie des mentions extraites dans {'decofre', 'ofcors', 'content'}.
            Par défaut, vaut `'ofcors'`.
        key_type : str, optional
            Le type des clés à utiliser dans le dictionnaire de sortie lorsque `mentions_format=='ofcors'`.
            Par défaut, vaut `"num"`.
        """      
        if mention_detector_model_path is not None:
            self.mention_detector_model_path = mention_detector_model_path
        else:
            self.mention_detector_model_path = get_path_of_file_in("ANCOR-EVERYTHING-2021-01-13-detector.model", "~")
        
        if output_dir_path is not None:
            self.output_dir_path = os.path.join(output_dir_path, "mentions_detection")
        else:
            self.output_dir_path = os.path.join(os.path.join(".", "ofcors_outputs"), "mentions_detection")
        
        self.mentions_format = mentions_format
        
        self.key_type = key_type
        
        if tokenizer=="spacy":
            self.nlp = spacy.load("fr_core_news_sm")
            self.tokenizer = tokenizer
        elif tokenizer=="stanza":
            self.nlp = stanza.Pipeline(processors='tokenize', lang='fr', verbose=False)
            self.tokenizer = tokenizer
        elif tokenizer=="spacy_stanza":
            self.nlp = spacy_stanza.load_pipeline("fr", verbose=False)
            self.tokenizer = tokenizer
        else:
            raise ValueError(f"Error : tokenizer value ({tokenizer}) not accepted.")    
        
    
    
    def __call__(self, text):
        """ Méthode __call__.
        
        Rend callable les instances de MentionDetector.
        
        Appelle simplement la méthode `self.predict` avec les mêmes arguments
        et retourne son résultat.
        Extrait et retourne les mentions à partir du texte décrit par `text`.
        
        Parameters
        ----------
        text : str
            La chaîne de caractère représentant le texte brut ou bien le chemin
            du fichier contenant le texte brut.
            
        Returns
        -------
        list of dict or dict of dict
            Les mentions extraites à partir de `text`.
        """
        return self.predict(text);
    
    
    
    @preconditions(lambda text: isinstance(text, str))          
    def predict(self, text):
        """ Méthode predict.
        
        Extrait et retourne les mentions à partir du texte décrit par `text`.
        
        Parameters
        ----------
        text : str
            La chaîne de caractère représentant le texte brut ou bien le chemin
            du fichier contenant le texte brut.
            
        Returns
        -------
        list of dict or dict of dict
            Les mentions extraites à partir de `text`.
        """
        # -- Contrôle de l'argument text
        if os.path.exists(text):
            if not os.path.isfile(text):
                raise ValueError(f"Le chemin ({text}) spécifié en argument ne désigne pas un fichier.")
            else:
                _raw_txt_path = text
        else :
            os.makedirs(self.output_dir_path, exist_ok=True)
            _raw_txt_path = os.path.join(self.output_dir_path, "raw_text.txt")
            with open(_raw_txt_path, "w") as raw_txt_file:
                raw_txt_file.write(text)
        
        # -- Calcul des mentions
        # Calcul des n-grams
        os.makedirs(self.output_dir_path, exist_ok=True)
        n_grams_file_path = os.path.join(self.output_dir_path, "built-ngrams.json")
        with open(_raw_txt_path, "r") as raw_text_file, open(os.path.join(self.output_dir_path, "built-ngrams.json"), "wb") as n_grams_file, open(os.path.join(self.output_dir_path, "tokens.json"), "w") as tokens_file:
            doc = self.nlp(raw_text_file.read())
            if self.tokenizer == "stanza":
                spans = list(ort.spans_from_doc(doc))
                token_dict = {str(i):token.text for i, token in enumerate(doc.iter_tokens())}
            else :
                spans = list(drt.spans_from_doc(doc))
                token_dict = {str(token.i):token.text for token in doc}
            
            n_grams_file.write(orjson.dumps(spans))
            
            # On stocke la liste des tokens dans un fichier à part, et non avec les mentions
            # car c'est une info qu'on ne conservera pas par la suite dans le pipeline de traitement.
            # Autant figer cette information dans un fichier, de sorte à ce qu'elle soit récupérable par la suite.
            json.dump(token_dict, tokens_file)
        
        
        # Calcul des mentions
        mentions_file_path = os.path.join(self.output_dir_path, "mentions.json")
        detmentions.main_entry_point(f'{self.mention_detector_model_path} {n_grams_file_path} {mentions_file_path}'.split())
        
        # -- Formatage des mentions
        with open(mentions_file_path, "r") as mentions_file:
            mentions = json.load(mentions_file)
        
        if self.mentions_format=='decofre':
            res = mentions.copy()
        elif self.mentions_format=='ofcors':
            # Conservation des n-grams qui sont des mentions seulement (aka sys_tag not None)
            filtered_mentions = [mention for mention in mentions if mention["sys_tag"] != None]
            
            # Conservation des features de mention utiles seulement
            feats_to_keep = ["content", "left_context", "right_context", "start", "end", "span_id"]
            mentions = [{str(k).upper():(str(v) if isinstance(v, int) else v) for k, v in mention.items() if str(k).lower() in feats_to_keep} 
                                for mention in filtered_mentions]
            
            '''
            # Filtrage des mentions incluses
            mentions = self.__filter_embedded_mentions(self.__filter_same_start_mentions(cleaned_mentions))
            '''
            if self.key_type == 'num':
                res = {str(i):mention for i, mention in enumerate(mentions, 1)};
            elif self.key_type == 'span_id':
                res = {str(mention["SPAN_ID"]):mention for mention in mentions};
            else:
                raise ValueError(f"Error : self.key_type value ({self.key_type}) not accepted.")
            
        elif self.mentions_format=='content':
            res = [' '.join(mention["content"]) for mention in mentions]
        else:
            raise ValueError(f"Error : mentions_format value ({self.mentions_format}) not accepted.")
        
        # -- Sauvegarde dans un fichier, et retour des mentions détectées
        with open(os.path.join(self.output_dir_path, "mentions_output.json"), "w") as mentions_only_file:
            json.dump(res, mentions_only_file)
        
        return res;
     
    '''
    def __filter_same_start_mentions(self, mentions_list):
        """
        Pour l'ensemble des mentions commencant au meme token, on ne garde que la plus longue.
        """
        filtered_mentions = []
        # On passe par un dictionnaire (qui est ordonné depuis python 3.7) pour conserver l'ordre des mentions
        # car un set() ne conserve par l'ordre. Un ordredset est un cas particulier de dict.
        starts_set = {m["START"]:"" for m in mentions_list}
        for start_value in starts_set:
            all_mentions_starting_at_start_value= list(filter(lambda m: int(m["START"])==int(start_value), mentions_list))
            longest_mention = max(all_mentions_starting_at_start_value, key=lambda m: int(m["END"]))
            filtered_mentions.append(longest_mention) 
        return filtered_mentions;
        
    
    def __filter_embedded_mentions(self, mentions_list):
        """
        Pour chaque mention, retire l'ensemble des mentions qui lui sont incluses.
        À la fin, on se retrouve avec la plus grande mention englobante.
        """
        filtered_mentions = mentions_list.copy()
        for mention in mentions_list:
            cur_start, cur_end = int(mention["START"]), int(mention["END"])
            # On retire toutes les mentions incluses dans la mention courante <=> garder que les mentions non-incluses avec filter
            filtered_mentions = list(filter(lambda m: ((m==mention) or not ((cur_start <= int(m["START"]) <= cur_end)
                                                                         and (cur_start <= int(m["END"]) <= cur_end))), filtered_mentions))
        return filtered_mentions;
    '''
