import os
import json
from preconditions import preconditions

class ChainsInterpreter():
    """ Classe ChainsInterpreter.
    
    Décrit un module d'interpretation des chaines de coréférences décrites par le système.
    
    Attributes
    ----------
    sorted_mentions_dict : dict
        Le dictionnaire ordonné des mentions à partir desquelles interpréter les chaines.
    
    """
    @preconditions(lambda sorted_mentions_dict: (isinstance(sorted_mentions_dict, (dict, os.PathLike)) or
                                                 sorted_mentions_dict is None)
                  )
    def __init__(self, sorted_mentions_dict=None, printer=True, with_context=True):
        """ Méthode __init__.
        
        Constructeur de ChainsInterpreter.
        
        Parameters
        ----------
        sorted_mentions_dict : dict or path-like object
            Le dictionnaire ordonné des mentions à partir desquelles interpréter les chaines.
            
        printer : bool
            Détermine si le module doit afficher ses sorties, ou seulement les retourner.
        """
        if sorted_mentions_dict is not None:
            if isinstance(sorted_mentions_dict, dict):
                self.sorted_mentions_dict = sorted_mentions_dict
            elif os.path.isfile(sorted_mentions_dict) and str(os.path.basename(sorted_mentions_dict)).endswith(".json"):
                with open(sorted_mentions_dict, "r") as dict_file:
                    self.sorted_mentions_dict = json.load(dict_file)
            else:
                raise ValueError("Error : The 'sorted_mentions_dict' argument should be either a dict object, or a json file path-like object.")
            
        else:
            path = os.path.join(os.path.join(os.path.join(".", "ofcors_outputs"), "mentions_detection"), "mentions_output.json")
            with open(path, "r") as dict_file:
                self.sorted_mentions_dict = json.load(dict_file)
                
        self.printer = printer
        
        self.with_context = with_context
        
   
            
    def __call__(self, chains):
        """ Méthode __call__.
        
        Rend callable les instances de ChainsInterpreter.
        
        Appelle simplement la méthode `self.predict` avec les mêmes arguments
        et retourne son résultat.
        Extrait et retourne les mentions à partir des chaînes décrites par le dictionnaire `chains`.
        
        Parameters
        ----------
        chains : dict
            Le dictionnaire des chaines (clusters) de coréférences, contenant pour chaque cluster
            la liste des noms des mentions qui le compose.
        
        Returns
        -------
        dict[str, list[tuple]]
            Le dictionnaire contenant pour chaque cluster, la liste des tuples contenant
            le contenu (et les contextes syntaxiques gauche et droit si `self.with_context`) de chaque mention.
        
        """
        return self.predict(chains);
        
        
    def predict(self, chains):
        """ Méthode predict.
        
        Extrait et retourne le contenu de chaque mention, dans son contexte,
        à partir des chaînes décrites par le dictionnaire `chains`.
        
        Parameters
        ----------
        chains : dict
            Le dictionnaire des chaines (clusters) de coréférences, contenant pour chaque cluster
            la liste des noms des mentions qui le compose.
            
        Returns
        -------
        dict[str, list[tuple]]
            Le dictionnaire contenant pour chaque cluster, la liste des tuples contenant
            le contenu (et les contextes syntaxiques gauche et droit si `self.with_context`) de chaque mention.
        
        """
        clusters_mentions_content = {}
        for cluster_name, cluster in chains.items():
            mentions_contents = []
            for mention_name in cluster:
                content = ' '.join(list(filter(lambda elt: elt!='<start>' and elt!='<end>',
                                               self.sorted_mentions_dict[str(mention_name)]["CONTENT"])))
                if self.with_context:
                    left_context = ' '.join(list(filter(lambda elt: elt!='<start>' and elt!='<end>',
                                                        self.sorted_mentions_dict[str(mention_name)]["LEFT_CONTEXT"])))
                    right_context = ' '.join(list(filter(lambda elt: elt!='<start>' and elt!='<end>',
                                                        self.sorted_mentions_dict[str(mention_name)]["RIGHT_CONTEXT"])))
                    t = (left_context, content, right_context)
                else:                                   
                    t = (content)
                    
                mentions_contents.append(t)
                
            if self.printer:
                print(f"Cluster {cluster_name}")
                if self.with_context:
                    print(f"{str('LEFT_CONTEXT'):<50} | {str('CONTENT'):<50} | {str('RIGHT_CONTEXT'):<50}")
                else:
                    print(f"{str('CONTENT')}")
                for t in mentions_contents:
                    if self.with_context:
                        print(f"{str(t[0]):<50} | {str(t[1]):<50} | {str(t[2]):<50}")
                    else:
                        print(f"{str(t)}")
                print()
            
            clusters_mentions_content[cluster_name] = mentions_contents

        return clusters_mentions_content;
